package main

import (
	"fmt"
	"net/http"

	"gitlab.com/ruangguru/source/user-auth/client/credit"

	"gitlab.com/ruangguru/source/user-auth/guestToken"

	"gitlab.com/ruangguru/source/user-auth/client/auth"
	"gitlab.com/ruangguru/source/user-auth/client/google"
	"gitlab.com/ruangguru/source/user-auth/client/grade"
	"gitlab.com/ruangguru/source/user-auth/client/region"
	"gitlab.com/ruangguru/source/user-auth/client/tokenValidation"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/ruangguru/source/shared-lib/go/healthcheck"
	"gitlab.com/ruangguru/source/shared-lib/go/logger"
	"gitlab.com/ruangguru/source/shared-lib/go/middleware"
	"gitlab.com/ruangguru/source/user-auth/api"
	"gitlab.com/ruangguru/source/user-auth/client/email"
	"gitlab.com/ruangguru/source/user-auth/client/facebook"
	"gitlab.com/ruangguru/source/user-auth/config"
	"gitlab.com/ruangguru/source/user-auth/user"
)

var log = logger.New()

const DBSingularTable = true

func InitDB(config config.Config) *gorm.DB {
	// init db connection
	dbUser := config.MysqlUser
	dbPass := config.MysqlPass
	dbEndpoint := config.MysqlHost
	dbPort := config.MysqlPort
	dbName := config.MysqlName
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbUser, dbPass, dbEndpoint, dbPort, dbName)

	db, err := gorm.Open("mysql", dsn)
	if err != nil {
		log.Fatalf(err.Error())
		panic(err)
	}

	db.SingularTable(DBSingularTable)

	if config.DBLogMode {
		db.SetLogger(log)
		db.LogMode(config.DBLogMode)
	}
	return db
}

func InitRouter(db *gorm.DB, config config.Config) *gin.Engine {
	// init repositories
	userRepo := user.NewRepository(db)

	// init services
	creditService := credit.NewService()
	gradeService := grade.NewService()
	regionService := region.NewService()
	remoteClientEmailService := email.NewService(config.RgBaseURL)
	facebookService := facebook.NewService()
	googleService := google.NewService()
	guestTokenService := guestToken.NewService()
	tokenValidationService := tokenValidation.NewService()
	remoteClientAuthService := auth.NewService(config.RgBaseURL, config.UserAPISecretKey)
	userService := user.NewService(userRepo, remoteClientEmailService, remoteClientAuthService, gradeService, creditService, regionService)
	middlewareService := middleware.NewMiddlewareService(remoteClientAuthService, config.InternalAPIRequestPassword)

	// init http handler
	authHandler := api.NewAuthHTTPHandler(userService, facebookService, googleService, tokenValidationService)
	guestTokenHandler := api.NewGuestTokenHTTPHandler(guestTokenService)
	userParentHandler := api.NewUserParentHTTPHandler(userService, remoteClientAuthService, config)
	userHandler := api.NewUserHTTPHandler(userService, remoteClientAuthService, regionService, config)
	adminHandler := api.NewAdminHTTPHandler(userService, config)

	allowedRoles := [7]string{
		"student",
		"teacher",
		"parent",
		"employee",
		"department",
		"company",
		"admin",
	}

	router := gin.New()
	router.Use(logger.GinRequestLogger(), middlewareService.RecoveryWithLogger(log), middlewareService.ApplyRequestID())
	hc := healthcheck.New()

	// declare endpoints
	entitiesRouter := router.Group("/")
	{
		entitiesRouter.GET("api/v3/user/resend-activation", userHandler.ResendActivation)
		entitiesRouter.POST("api/v3/user/check-username-cellphone", userHandler.CheckAvailableEmailAndCellphoneNumber)
		entitiesRouter.POST("api/v3/user", userHandler.Register)
		entitiesRouter.POST("api/v3/user/forgot-password", userHandler.SetForgotPassword)
		entitiesRouter.GET("api/v3/user/search", userHandler.SearchUserByEmail)
		entitiesRouter.GET("api/v3/user/profile/:userId", userHandler.GetUserDataByUserId)
		entitiesRouter.GET("api/v3/user/users_profiles_by_emails", userHandler.SearchUserByEmails)
		entitiesRouter.GET("api/v3/user/users_profiles_by_uocs", userHandler.SearchUserByUocs)
		entitiesRouter.GET("api/v3/user/users_profiles_by_user_ids", userHandler.SearchUserByUserIDs)
		entitiesRouter.GET("api/v3/user/forgot-password", userHandler.GetForgotPassword)
		entitiesRouter.GET("api/v3/user/activate-user-by-email", userHandler.ActivateUserByEmail)
		entitiesRouter.POST("api/v3/auth", authHandler.Login)
		entitiesRouter.POST("api/v3/auth/token", guestTokenHandler.GenerateGuestToken)
		entitiesRouter.POST("api/v3/auth/refresh-token", authHandler.RefreshToken)
		entitiesRouter.GET("/", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{"service_name": "user-auth", "status": "success", "message": "it works"})
		})
		entitiesRouter.GET("_health", hc.GinHandler())
	}
	AuthenticatedRoute := router.Group("/api/v3/user")
	AuthenticatedRoute.Use(middlewareService.AuthenticateRequest())
	AuthenticatedRoute.Use(middlewareService.AuthorizeRole(allowedRoles[0:7]))
	{
		AuthenticatedRoute.GET("/parent/tnc/check-agreement", userParentHandler.CheckTncAgreement)
		AuthenticatedRoute.GET("/profile-percentage", userHandler.GetUserPercentage)
		AuthenticatedRoute.GET("/check_activation", userHandler.CheckUserActive)
		AuthenticatedRoute.GET("/parent/check", userHandler.CheckUserParent)
		AuthenticatedRoute.POST("/parent", userHandler.CreateUserParent)
		AuthenticatedRoute.POST("/parent/tnc/submit", userParentHandler.SubmitTncAgreement)
		AuthenticatedRoute.POST("/subscribe", userHandler.UpdateUserSubcribe)
		AuthenticatedRoute.GET("", userHandler.GetUserDataFromToken)
		AuthenticatedRoute.PUT("", userHandler.UpdateUser)
		AuthenticatedRoute.GET("/find", userHandler.GetUserDataFromUocOrReferralCode)
		AuthenticatedRoute.PUT("/change-password", userHandler.ChangePassword)
	}
	AdminRoute := router.Group("/api/v3/admin")
	AdminRoute.Use(middlewareService.AuthenticateRequest())
	AdminRoute.Use(middlewareService.AuthorizeRole(allowedRoles[5:7]))
	{
		AdminRoute.POST("/users/change-email", adminHandler.ChangeUserEmail)
		AdminRoute.POST("/users/ruker/bulk", adminHandler.CreateUserRukerCMSBulk)
		AdminRoute.GET("/users", adminHandler.GetUsersByQuery)
		AdminRoute.GET("/users/all", adminHandler.GetAllUsersByQuery)
		AdminRoute.GET("/user/:userID", adminHandler.GetUserById)
		AdminRoute.PUT("/user/:userID", adminHandler.UpdateUserById)
		AdminRoute.PUT("/users", adminHandler.UpdateUsers)
	}
	return router
}

func main() {
	config := config.Get()

	db := InitDB(config)
	defer db.Close()

	router := InitRouter(db, config)

	if err := router.Run(); err != nil {
		panic(fmt.Errorf("Failed starting server: %s", err))
	}
}
