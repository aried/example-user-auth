package user

import (
	"context"
	"crypto/md5"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/ruangguru/source/shared-lib/go/utils/encrypt"
	"gitlab.com/ruangguru/source/shared-lib/go/utils/serial"
	"gitlab.com/ruangguru/source/user-auth/client/auth"
	"gitlab.com/ruangguru/source/user-auth/client/credit"
	emailClient "gitlab.com/ruangguru/source/user-auth/client/email"
	"gitlab.com/ruangguru/source/user-auth/client/grade"
	"gitlab.com/ruangguru/source/user-auth/client/region"
	"gitlab.com/ruangguru/source/user-auth/config"
	customfault "gitlab.com/ruangguru/source/user-auth/fault"

	//"gitlab.com/ruangguru/source/user-auth/fault"
	"gitlab.com/ruangguru/source/shared-lib/go/fault"
	"gitlab.com/ruangguru/source/user-auth/utils/mysqlutil"
	serialutil "gitlab.com/ruangguru/source/user-auth/utils/serialutil"
	validationutil "gitlab.com/ruangguru/source/user-auth/utils/validationutil"
)

type Service interface {
	GetUser(email string) (*Users, error)
	BuildTokenClaims(string, *Users, LoginReq, bool) jwt.MapClaims
	BuildRefreshTokenClaims(*Users, LoginReq, bool) jwt.MapClaims
	BuildFirebaseTokenClaims(*Users) jwt.MapClaims
	CreateToken(jwt.MapClaims) (string, error)
	CreateRefreshToken(jwt.MapClaims) (string, error)
	CreateFirebaseToken(jwt.MapClaims) (string, error)
	BuildResponse(string, string, string, LoginReq, *Users) *ResponseData
	FillInRequest(*LoginReq)
	CreateUser(*Users, *UserExtra) error
	GetUserParentData(context.Context, int) (*UserParent, error)
	GetUserPercentage(ctx context.Context, userID int) (percentage int, err error)
	CheckUserParent(ctx context.Context, userID int) (bool, error)
	CheckUserActivation(userID int) (UserActivationStatus, error)
	ResendActivation(email string) error
	RefreshToken(refreshToken RefreshTokenReq) (RefreshTokenResponse, error)
	UpdateLastCheckParentalForm(context.Context, int) error
	CreateUserParent(context.Context, UserParent) error
	UpdateTnCAgreed(context.Context, int, string, string) error
	UpdateUserSubcribe(ctx context.Context, userID int, subscribe bool) error
	GetUsersByQuery(context.Context, GetUserQuery) (map[string]Users, error)
	GetUsersByUserIDs(ctx context.Context, query GetUserQuery) (map[int]Users, error)
	GetUsersByEmailOrCellphoneNumber(email string, cellphoneNumber string) (users Users, err error)
	UpdateUsersByAdmin(context.Context, []UpdateUsersByAdminQuery) error
	CreateUserRukerCMSBulk(ctx context.Context, payload []CreateUserRukerCMSBulkReq) ([]CreateUserRukerCMSBulkResp, error)
	GetUserByEmail(ctx context.Context, email string) (users Users, err error)
	GetUserByCellphoneNumber(ctx context.Context, cellphoneNumber string) (users Users, err error)
	Register(ctx context.Context, req RegisterReq, userCode string) (RegisterResp, error)
	GetUserByUserIDForUserProfile(ctx context.Context, userID string) (GetUserProfile, error)
	GetUserByUocOrReferralCode(ctx context.Context, uoc string) (GetUserProfileFromUsersTableResp, error)
	GetUserByUserId(ctx context.Context, userId string) (GetUserProfileFromUsersTableResp, error)
	SearchUserByEmail(ctx context.Context, email string) (GetUserProfileFromUsersTableResp, error)
	GetUsersByEmails(ctx context.Context, query GetUserQuery) (map[string]Users, error)
	ChangePassword(ctx context.Context, userID int, payload ChangePasswordReq) (err error)
	GetForgotPassword(ctx context.Context, email string, isRuker bool) (key string, err error)
	SetForgotPassword(ctx context.Context, payload ForgotPasswordReq) (err error)
	ChangeUserEmail(ctx context.Context, payload ChangeUserEmailReq) error
	ActivateUserByEmail(ctx context.Context, email string, activationCode string) error
	UpdateUser(ctx context.Context, payload UpdateFormRequest, userId string) (GetUserProfile, error)
	GetAllUsersByQuery(ctx context.Context, query GetAllUsersQuery) ([]GetAllUsersByQueryResponse, error)
}

type service struct {
	repo           Repository
	emailClientSvc emailClient.Service
	authSvc        auth.Service
	gradeSvc       grade.Service
	creditSvc      credit.Service
	regionSvc      region.Service
}

func NewService(repo Repository, emailClientSvc emailClient.Service, authSvc auth.Service, gradeSvc grade.Service, creditSvc credit.Service, regionSvc region.Service) Service {
	return &service{
		repo:           repo,
		emailClientSvc: emailClientSvc,
		authSvc:        authSvc,
		gradeSvc:       gradeSvc,
		creditSvc:      creditSvc,
		regionSvc:      regionSvc,
	}
}

var conf = config.Get()

type GradeNotFoundError struct {
	Msg string
}

func (e *GradeNotFoundError) Error() string {
	return e.Msg
}

type ActivationCodeNotMatchError struct {
	Msg string
}

func (e *ActivationCodeNotMatchError) Error() string {
	return e.Msg
}

func (s *service) GetAllUsersByQuery(ctx context.Context, query GetAllUsersQuery) ([]GetAllUsersByQueryResponse, error) {
	users, err := s.repo.GetAllUsersByQuery(ctx, query)
	if err != nil {
		return nil, err
	}
	// cityRange := makeRange(1, 700)
	// city, err := s.regionSvc.GetCity(cityRange)
	// if err != nil {
	// 	return nil, err
	// }
	// usersMap := make(map[string]GetAllUsersByQueryResponse, len(users))
	var userResponse GetAllUsersByQueryResponse
	var response []GetAllUsersByQueryResponse
	for _, user := range users {
		userResponse.ID = strconv.Itoa(user.ID)
		userResponse.Name = user.Name
		userResponse.Email = user.Email
		userResponse.Role = user.Role
		userResponse.CellphoneNumber = user.CellphoneNumber
		userResponse.IsActive = user.IsActive
		userResponse.UniqueOrderCode = user.UniqueOrderCode
		userResponse.CreatedAt = user.CreatedAt.Format("2006-01-02")
		userResponse.Gender = user.Gender
		userResponse.ProfilePic = getProfilePicFullString(user.ProfilePic)
		userResponse.GradeSerial = user.GradeSerial
		// userResponse.DomicileCity = getUserDomicileCity(city, user.DomicileCity)
		userResponse.DomicileCity = user.DomicileCity
		// usersMap[userResponse.ID] = userResponse
		response = append(response, userResponse)
	}

	return response, err
}

// func getUserDomicileCity(city region.CityResponse, domicileCity *int) string {
// 	if domicileCity == nil {
// 		return ""
// 	}
// 	if _, ok := city.Data[strconv.Itoa(*domicileCity)]; ok {
// 		return city.Data[strconv.Itoa(*domicileCity)].DisplayName
// 	}
// 	return ""
// }

func getProfilePicFullString(profpic *string) string {
	s3BaseUrl := "https://core-ruangguru.s3.amazonaws.com/"
	defaultProfPic := s3BaseUrl + "assets/avatar/avatar|2599.png"
	var profilePicFull string
	if profpic == nil {
		profilePicFull = defaultProfPic
	} else {
		profilePicFull = s3BaseUrl + *profpic
	}
	return profilePicFull
}

func makeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}

func processNewUserTableValues(payload UpdateFormRequest, user *Users, gradeSvc grade.Service) (bool, error) {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			user.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.CellphoneNumber != "" {
		if !validationutil.ValidateCellphoneNumber(payload.CellphoneNumber) {
			return false, customfault.InvalidFormatError{"cellphone_number", "Nomor HP harus dimulai dengan 62 dan minimal 10 digit angka, maksimal 17 angka"}
		}
		user.CellphoneNumber = payload.CellphoneNumber
		needsUpdate = true
	}
	if payload.Name != "" {
		user.Name = payload.Name
		needsUpdate = true
	}
	if payload.Gender != "" {
		if !validationutil.ValidateGender(payload.Gender) {
			return false, customfault.InvalidFormatError{"gender", "Allowed Value: 'F', 'M'"}
		}
		user.Gender = payload.Gender
		needsUpdate = true
	}
	if payload.BirthDate != "" {
		t, err := time.Parse("2006-01-02", payload.BirthDate)
		if err != nil {
			return false, customfault.InvalidFormatError{"birth_date", "Format tanggal harus YYYY-MM-DD"}
		}
		user.BirthDate = &t
		needsUpdate = true
	}
	if payload.BirthPlace != nil {
		user.BirthPlace = payload.BirthPlace
		needsUpdate = true
	}
	if payload.LessonName != nil {
		gradeResponse, err := gradeSvc.GetGrade(*payload.LessonName)
		if err != nil {
			return false, err
		}
		if gradeResponse.Message == "No record(s) found!" {
			return false, customfault.InvalidValueError{"lesson_name", "lesson_name value is not allowed"}
		}
		user.GradeSerial = gradeResponse.Data[0].Serial
		needsUpdate = true
	}
	user.UpdatedAt = time.Now()
	return needsUpdate, nil
}

func processNewUserAddressTableValues(payload UpdateFormRequest, userAddress *UserAddress) bool {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			userAddress.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.DomicileAddress != nil {
		userAddress.DomicileAddress = payload.DomicileAddress
		needsUpdate = true
	}
	if payload.DomicileCity != nil {
		userAddress.DomicileCity = payload.DomicileCity
		needsUpdate = true
	}
	if payload.DomicileProvince != nil {
		userAddress.DomicileProvince = payload.DomicileProvince
		needsUpdate = true
	}
	if payload.DomicileKecamatan != nil {
		userAddress.DomicileKecamatan = payload.DomicileKecamatan
		needsUpdate = true
	}
	if payload.DomicilePostalCode != nil {
		userAddress.DomicilePostalCode = payload.DomicilePostalCode
		needsUpdate = true
	}
	if payload.KtpAddress != "" {
		userAddress.KtpAddress = payload.KtpAddress
		needsUpdate = true
	}
	if payload.KtpCity != nil {
		userAddress.KtpCity = payload.KtpCity
		needsUpdate = true
	}
	if payload.KtpProvince != nil {
		userAddress.KtpProvince = payload.KtpProvince
		needsUpdate = true
	}
	if payload.KtpKecamatan != nil {
		userAddress.KtpKecamatan = payload.KtpKecamatan
		needsUpdate = true
	}
	if payload.KtpPostalCode != nil {
		userAddress.KtpPostalCode = payload.KtpPostalCode
		needsUpdate = true
	}
	userAddress.UpdatedAt = time.Now()
	return needsUpdate
}

func processNewUserParentTableValues(payload UpdateFormRequest, userParent *UserParent) (bool, error) {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			userParent.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.ParentCellphoneNumber != "" {
		userParent.ParentCellphoneNumber = payload.ParentCellphoneNumber
		needsUpdate = true
	}
	if payload.ParentName != "" {
		userParent.ParentName = payload.ParentName
		needsUpdate = true
	}
	return needsUpdate, nil
}

func processNewUserExtraTableValues(payload UpdateFormRequest, userExtra *UserExtra) (bool, error) {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			userExtra.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.LessonName != nil {
		userExtra.LessonName = payload.LessonName
		needsUpdate = true
	}
	if payload.Degree != nil {
		userExtra.Degree = payload.Degree
		needsUpdate = true
	}
	if payload.SchoolName != nil {
		userExtra.SchoolName = payload.SchoolName
		needsUpdate = true
	}
	if payload.Nisn != nil {
		userExtra.Nisn = payload.Nisn
		needsUpdate = true
	}
	if payload.Npsn != nil {
		userExtra.Npsn = payload.Npsn
		needsUpdate = true
	}
	if payload.LastUrl != nil {
		userExtra.LastUrl = payload.LastUrl
		needsUpdate = true
	}
	if payload.SchoolName != nil {
		userExtra.SchoolName = payload.SchoolName
		needsUpdate = true
	}
	if payload.Source != nil {
		userExtra.Source = payload.Source
		needsUpdate = true
	}
	if payload.AdminCode != nil {
		userExtra.AdminCode = payload.AdminCode
		needsUpdate = true
	}
	if payload.IdAdminProfile != nil {
		userExtra.IdAdminProfile = payload.IdAdminProfile
		needsUpdate = true
	}
	if payload.IsParent != "" {
		b, err := strconv.ParseBool(payload.IsParent)
		if err != nil {
			return false, customfault.InvalidTypeError{"is_parent", "boolean"}
		}
		userExtra.IsParent = b
		needsUpdate = true
	}
	if payload.Subscribe != nil {
		userExtra.Subscribe = payload.Subscribe
		needsUpdate = true
	}
	if payload.FacebookID != nil {
		userExtra.FacebookID = payload.FacebookID
		needsUpdate = true
	}
	if payload.ReferralCode != nil {
		userExtra.ReferralCode = payload.ReferralCode
		needsUpdate = true
	}
	if payload.CellphoneNumber_2 != nil {
		userExtra.CellphoneNumber_2 = payload.CellphoneNumber_2
		needsUpdate = true
	}
	if payload.OfficePhoneNumber != nil {
		userExtra.OfficePhoneNumber = payload.OfficePhoneNumber
		needsUpdate = true
	}
	if payload.CallStatus != nil {
		userExtra.CallStatus = payload.CallStatus
		needsUpdate = true
	}
	if payload.CallProgress != nil {
		userExtra.CallProgress = payload.CallProgress
		needsUpdate = true
	}
	if payload.MuridHandleBy != nil {
		userExtra.MuridHandleBy = payload.MuridHandleBy
		needsUpdate = true
	}
	if payload.CampaignCode != nil {
		userExtra.CampaignCode = payload.CampaignCode
		needsUpdate = true
	}
	if payload.Preference != nil {
		userExtra.Preference = payload.Preference
		needsUpdate = true
	}
	if payload.SourceInfoId != nil {
		userExtra.SourceInfoId = payload.SourceInfoId
		needsUpdate = true
	}
	userExtra.UpdatedAt = time.Now()
	return needsUpdate, nil
}

func processNewUserAdministrativeTableValues(payload UpdateFormRequest, userAdministrative *UserAdministrative) bool {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			userAdministrative.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.IdIdentityType != nil {
		userAdministrative.IdIdentityType = payload.IdIdentityType
		needsUpdate = true
	}
	if payload.NikNo != nil {
		userAdministrative.NikNo = payload.NikNo
		needsUpdate = true
	}
	if payload.NikVerified != nil {
		userAdministrative.NikVerified = payload.NikVerified
		needsUpdate = true
	}
	if payload.NpwpNo != nil {
		userAdministrative.NpwpNo = payload.NpwpNo
		needsUpdate = true
	}
	if payload.TaxMonth != nil {
		userAdministrative.TaxMonth = payload.TaxMonth
		needsUpdate = true
	}
	if payload.TaxYear != nil {
		userAdministrative.TaxYear = payload.TaxYear
		needsUpdate = true
	}
	if payload.NpwpImageVerified != nil {
		userAdministrative.NpwpImageVerified = payload.NpwpImageVerified
		needsUpdate = true
	}
	if payload.TaxNoteVerified != nil {
		userAdministrative.TaxNoteVerified = payload.TaxNoteVerified
		needsUpdate = true
	}
	if payload.PernyataanPenghasilan != nil {
		userAdministrative.PernyataanPenghasilan = payload.PernyataanPenghasilan
		needsUpdate = true
	}
	if payload.BankId != nil {
		userAdministrative.BankId = payload.BankId
		needsUpdate = true
	}
	if payload.BankBranch != nil {
		userAdministrative.BankBranch = payload.BankBranch
		needsUpdate = true
	}
	if payload.GuruBankRekening != nil {
		userAdministrative.GuruBankRekening = payload.GuruBankRekening
		needsUpdate = true
	}
	if payload.GuruBankPemilik != nil {
		userAdministrative.GuruBankPemilik = payload.GuruBankPemilik
		needsUpdate = true
	}
	userAdministrative.UpdatedAt = time.Now()
	return needsUpdate
}

func processNewUserTeacherTableValues(payload UpdateFormRequest, userTeacher *UserTeacher) (bool, error) {
	needsUpdate := false
	if payload.DeletedAt != nil {
		if *payload.DeletedAt != false {
			now := time.Now()
			userTeacher.DeletedAt = &now
			needsUpdate = true
		}
	}
	if payload.PendidikanId != nil {
		userTeacher.PendidikanId = payload.PendidikanId
		needsUpdate = true
	}
	if payload.GuruPendidikanInstansi != nil {
		userTeacher.GuruPendidikanInstansi = payload.GuruPendidikanInstansi
		needsUpdate = true
	}
	if payload.GuruPendidikanVerified != nil {
		userTeacher.GuruPendidikanVerified = payload.GuruPendidikanVerified
		needsUpdate = true
	}
	if payload.GuruTwitter != nil {
		userTeacher.GuruTwitter = payload.GuruTwitter
		needsUpdate = true
	}
	if payload.GuruReferral != nil {
		userTeacher.GuruReferral = payload.GuruReferral
		needsUpdate = true
	}
	if payload.KategoriId != nil {
		userTeacher.KategoriId = payload.KategoriId
		needsUpdate = true
	}
	if payload.GuruRating != nil {
		userTeacher.GuruRating = payload.GuruRating
		needsUpdate = true
	}
	if payload.GuruIspro != nil {
		userTeacher.GuruIspro = payload.GuruIspro
		needsUpdate = true
	}
	if payload.LongAboutMe != nil {
		userTeacher.GuruBio = payload.LongAboutMe
		needsUpdate = true
	}
	if payload.GuruReview != nil {
		userTeacher.GuruReview = payload.GuruReview
		needsUpdate = true
	}
	if payload.GuruKualifikasi != nil {
		userTeacher.GuruKualifikasi = payload.GuruKualifikasi
		needsUpdate = true
	}
	if payload.GuruPengalaman != nil {
		userTeacher.GuruPengalaman = payload.GuruPengalaman
		needsUpdate = true
	}
	if payload.GuruMetode != nil {
		userTeacher.GuruMetode = payload.GuruMetode
		needsUpdate = true
	}
	if payload.GuruBlocked != nil {
		userTeacher.GuruBlocked = payload.GuruBlocked
		needsUpdate = true
	}
	if payload.GuruLastLogin != "" {
		t, err := time.Parse("2006-01-02", payload.GuruLastLogin)
		if err != nil {
			return false, customfault.InvalidFormatError{"guru_last_login", "Format tanggal harus YYYY-MM-DD"}
		}
		userTeacher.GuruLastLogin = &t
		needsUpdate = true
	}
	if payload.JenjangPendidikan != nil {
		userTeacher.JenjangPendidikan = payload.JenjangPendidikan
		needsUpdate = true
	}
	if payload.TeachOnline != nil {
		userTeacher.TeachOnline = payload.TeachOnline
		needsUpdate = true
	}
	if payload.IsThebest != nil {
		userTeacher.IsThebest = payload.IsThebest
		needsUpdate = true
	}
	if payload.RatingSummary != "" {
		value, err := strconv.ParseFloat(payload.RatingSummary, 32)
		if err != nil {
			return false, customfault.InvalidTypeError{"rating_summary", "float"}
		}
		userTeacher.RatingSummary = float32(value)
		needsUpdate = true
	}
	if payload.OverallRating != nil {
		userTeacher.OverallRating = payload.OverallRating
		needsUpdate = true
	}
	if payload.IsVerified != "" {
		b, err := strconv.ParseBool(payload.IsVerified)
		if err != nil {
			return false, customfault.InvalidTypeError{"is_verified", "boolean"}
		}
		userTeacher.IsVerified = b
		needsUpdate = true
	}
	if payload.ShortAboutMe != nil {
		userTeacher.BioSingkat = payload.ShortAboutMe
		needsUpdate = true
	}
	if payload.TagLine != nil {
		userTeacher.TagLine = payload.TagLine
		needsUpdate = true
	}
	userTeacher.UpdatedAt = time.Now()
	return needsUpdate, nil
}

func (s *service) UpdateUser(ctx context.Context, payload UpdateFormRequest, userId string) (GetUserProfile, error) {
	user, userAddress, userAdministrative, userExtra, userTeacher, userParent, err := s.repo.GetUserByUserIDForUserProfile(ctx, userId)
	if err != nil {
		return GetUserProfile{}, err
	}
	if user.ID == 0 {
		err = fault.ErrorDictionary(fault.UserHadBeenDeleted, "")
		return GetUserProfile{}, err
	}
	userTableNeedsUpdate, err := processNewUserTableValues(payload, user, s.gradeSvc)
	if err != nil {
		return GetUserProfile{}, err
	}
	userParentTableNeedsUpdate, err := processNewUserParentTableValues(payload, userParent)
	if err != nil {
		return GetUserProfile{}, err
	}
	userTeacherTableNeedsUpdate, err := processNewUserTeacherTableValues(payload, userTeacher)
	if err != nil {
		return GetUserProfile{}, err
	}
	userExtraTableNeedsUpdate, err := processNewUserExtraTableValues(payload, userExtra)
	if err != nil {
		return GetUserProfile{}, err
	}
	userAddressTableNeedsUpdate := processNewUserAddressTableValues(payload, userAddress)
	userAdministrativeTableNeedsUpdate := processNewUserAdministrativeTableValues(payload, userAdministrative)

	studentClassResponse, err := s.gradeSvc.GetStudentClass()
	if err != nil {
		return GetUserProfile{}, err
	}
	userTableNeedsUpdate = true
	userExtraTableNeedsUpdate = true
	var curriculumSerial string
	var curriculumName string
	var curriculumSerialDefault string
	var curriculumNameDefault string
	for _, studentClass := range studentClassResponse.Data {
		if studentClass.Serial == user.GradeSerial && len(studentClass.Options) > 0 {
			curriculumSerial = studentClass.Options[0].Serial
			curriculumName = studentClass.Options[0].Name
			if payload.CurriculumSerial != nil {
				for _, option := range studentClass.Options {
					if option.Serial == *payload.CurriculumSerial {
						curriculumSerial = option.Serial
						curriculumName = option.Name
					}
				}
			}
		}

		if studentClass.Serial == "kelas-ix" && curriculumSerial == "" {
			curriculumSerialDefault = studentClass.Options[0].Serial
			curriculumNameDefault = studentClass.Options[0].Name
		}
	}
	user.CurriculumSerial = curriculumSerial
	userExtra.CurriculumName = &curriculumName
	if curriculumSerial == "" {
		user.CurriculumSerial = curriculumSerialDefault
		userExtra.CurriculumName = &curriculumNameDefault
	}

	updateTableNeeded := make(map[string]bool)
	updateTableNeeded["userTable"] = userTableNeedsUpdate
	updateTableNeeded["userAddressTable"] = userAddressTableNeedsUpdate
	updateTableNeeded["userAdministrativeTable"] = userAdministrativeTableNeedsUpdate
	updateTableNeeded["userExtraTable"] = userExtraTableNeedsUpdate
	updateTableNeeded["userTeacherTable"] = userTeacherTableNeedsUpdate
	updateTableNeeded["userParentTable"] = userParentTableNeedsUpdate

	if updateTableNeeded["userTable"] || updateTableNeeded["userAddressTable"] || updateTableNeeded["userAdministrativeTable"] || updateTableNeeded["userExtraTable"] || updateTableNeeded["userTeacherTable"] || updateTableNeeded["userParentTable"] {
		err = s.repo.UpdateUserAllTables(ctx, user, userAddress, userAdministrative, userExtra, userTeacher, userParent, updateTableNeeded)
		if err != nil {
			return GetUserProfile{}, err
		}
	}

	var resp GetUserProfile
	processReturnDataFromTableUsers(&resp, user)
	processReturnDataFromTableUserTeacher(&resp, userTeacher)
	processReturnDataFromTableUserExtra(&resp, userExtra)
	processReturnDataFromTableUserAdministrative(&resp, userAdministrative)
	processReturnDataFromTableUserParent(&resp, userParent)
	err = processReturnDataFromTableUserAddress(&resp, userAddress, s.regionSvc)
	if err != nil {
		return GetUserProfile{}, err
	}
	return resp, nil
}

func (s *service) ActivateUserByEmail(ctx context.Context, email string, activationCode string) error {
	user, err := s.repo.GetUser(email)
	if err != nil {
		return err
	}
	if user.ActivatedCode != activationCode {
		return &ActivationCodeNotMatchError{"Activation code does not match with database"}
	}
	err = s.repo.ActivateUserByEmail(ctx, user)
	if err != nil {
		return err
	}
	if conf.IsKoinAddedOnActivation {
		err = s.creditSvc.AddKoin(user.ID, 1)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *service) GetUsersByEmails(ctx context.Context, query GetUserQuery) (map[string]Users, error) {
	users, err := s.repo.GetUsersByQuery(ctx, query)
	if err != nil {
		return nil, err
	}
	usersMap := make(map[string]Users, len(users))
	for _, user := range users {
		usersMap[user.Email] = user
	}
	return usersMap, err
}

func (s *service) SearchUserByEmail(ctx context.Context, email string) (GetUserProfileFromUsersTableResp, error) {
	user, err := s.repo.GetUser(email)
	if err != nil {
		return GetUserProfileFromUsersTableResp{}, err
	}
	profileFromUsersTable := GetUserProfile{}
	processReturnDataFromTableUsers(&profileFromUsersTable, user)
	getUserProfileFromUsersTableResp := GetUserProfileFromUsersTableResp{}
	turnProfileToResponseUsersTable(&profileFromUsersTable, &getUserProfileFromUsersTableResp)
	return getUserProfileFromUsersTableResp, nil
}

func (s *service) GetUserByUserId(ctx context.Context, userId string) (GetUserProfileFromUsersTableResp, error) {
	userIdInt, err := strconv.Atoi(userId)
	if err != nil {
		return GetUserProfileFromUsersTableResp{}, err
	}
	user, err := s.repo.GetUserByUserID(userIdInt)
	if err != nil {
		return GetUserProfileFromUsersTableResp{}, err
	}
	profileFromUsersTable := GetUserProfile{}
	processReturnDataFromTableUsers(&profileFromUsersTable, user)
	getUserProfileFromUsersTableResp := GetUserProfileFromUsersTableResp{}
	turnProfileToResponseUsersTable(&profileFromUsersTable, &getUserProfileFromUsersTableResp)
	return getUserProfileFromUsersTableResp, nil
}

func (s *service) GetUserByUocOrReferralCode(ctx context.Context, uoc string) (GetUserProfileFromUsersTableResp, error) {
	user, err := s.repo.GetUserByUocOrReferralCode(ctx, uoc)
	if err != nil {
		return GetUserProfileFromUsersTableResp{}, err
	}
	profileFromUsersTable := GetUserProfile{}
	processReturnDataFromTableUsers(&profileFromUsersTable, user)
	getUserProfileFromUsersTableResp := GetUserProfileFromUsersTableResp{}
	turnProfileToResponseUsersTable(&profileFromUsersTable, &getUserProfileFromUsersTableResp)
	return getUserProfileFromUsersTableResp, nil
}

func turnProfileToResponseUsersTable(profileFromUsersTable *GetUserProfile, getUserProfileFromUsersTableResp *GetUserProfileFromUsersTableResp) {
	getUserProfileFromUsersTableResp.ActivationCode = ""
	getUserProfileFromUsersTableResp.BirthDate = profileFromUsersTable.BirthDate
	getUserProfileFromUsersTableResp.BirthPlace = profileFromUsersTable.BirthPlace
	getUserProfileFromUsersTableResp.CellphoneNumber = profileFromUsersTable.CellphoneNumber
	getUserProfileFromUsersTableResp.CreatedAt = profileFromUsersTable.CreatedAt
	getUserProfileFromUsersTableResp.Email = profileFromUsersTable.Email
	getUserProfileFromUsersTableResp.Gender = profileFromUsersTable.Gender
	getUserProfileFromUsersTableResp.GradeSerial = profileFromUsersTable.GradeSerial
	getUserProfileFromUsersTableResp.IsActive = profileFromUsersTable.IsActive
	getUserProfileFromUsersTableResp.Name = profileFromUsersTable.Name
	getUserProfileFromUsersTableResp.ParentalCode = profileFromUsersTable.ParentalCode
	getUserProfileFromUsersTableResp.ProfilePic = profileFromUsersTable.ProfilePic
	getUserProfileFromUsersTableResp.ProfilePicFullDomain = profileFromUsersTable.ProfilePicFullDomain
	getUserProfileFromUsersTableResp.ProfpicWithoutKey = profileFromUsersTable.ProfpicWithoutKey
	getUserProfileFromUsersTableResp.QiscusEmail = profileFromUsersTable.QiscusEmail
	getUserProfileFromUsersTableResp.ReferralCode = profileFromUsersTable.ReferralCode
	getUserProfileFromUsersTableResp.Role = profileFromUsersTable.Role
	getUserProfileFromUsersTableResp.Shortname = profileFromUsersTable.Shortname
	getUserProfileFromUsersTableResp.UniqueOrderCode = profileFromUsersTable.UniqueOrderCode
}

func (s *service) GetUserByUserIDForUserProfile(ctx context.Context, userID string) (GetUserProfile, error) {
	user, userAddress, userAdministrative, userExtra, userTeacher, userParent, err := s.repo.GetUserByUserIDForUserProfile(ctx, userID)
	if err != nil {
		return GetUserProfile{}, err
	}
	var resp GetUserProfile
	processReturnDataFromTableUsers(&resp, user)
	processReturnDataFromTableUserTeacher(&resp, userTeacher)
	processReturnDataFromTableUserExtra(&resp, userExtra)
	processReturnDataFromTableUserAdministrative(&resp, userAdministrative)
	processReturnDataFromTableUserParent(&resp, userParent)
	err = processReturnDataFromTableUserAddress(&resp, userAddress, s.regionSvc)
	if err != nil {
		return GetUserProfile{}, err
	}

	if *resp.CurriculumSerial == "" {
		studentClassResponse, err := s.gradeSvc.GetStudentClass()
		if err != nil {
			return GetUserProfile{}, err
		}
		for _, studentClass := range studentClassResponse.Data {
			if studentClass.Serial == resp.GradeSerial && len(studentClass.Options) > 0 {
				resp.CurriculumSerial = &studentClass.Options[0].Serial
				resp.CurriculumName = &studentClass.Options[0].Name
			}
		}
	}
	return resp, nil
}

func processReturnDataFromTableUserParent(resp *GetUserProfile, userParent *UserParent) {
	resp.ParentName = userParent.ParentName
	resp.ParentCellphoneNumber = userParent.ParentCellphoneNumber
}

func processReturnDataFromTableUserAdministrative(resp *GetUserProfile, userAdministrative *UserAdministrative) {
	resp.NikNo = userAdministrative.NikNo
	resp.NikImage = userAdministrative.NikImage
	resp.NpwpNo = userAdministrative.NpwpNo
	resp.NpwpImage = userAdministrative.NpwpImage
	resp.BankId = userAdministrative.BankId
	resp.BankName = userAdministrative.BankBranch
	resp.BankAccountNo = userAdministrative.GuruBankRekening
	resp.BankAccountName = userAdministrative.GuruBankPemilik
}

func processReturnDataFromTableUserExtra(resp *GetUserProfile, userExtra *UserExtra) {
	resp.CreditBalance = userExtra.CreditBalance
	resp.Subscribe = 1
	resp.IsParent = 0
	resp.OfficePhoneNumber = userExtra.OfficePhoneNumber
	resp.CellphoneNumber2 = userExtra.CellphoneNumber_2
	resp.Degree = userExtra.Degree
	resp.SchoolName = userExtra.SchoolName
	resp.LastUrl = userExtra.LastUrl

	var lessonName *string
	if userExtra.LessonName != nil {
		r := regexp.MustCompile("-")
		tempLessonName := r.ReplaceAllString(*userExtra.LessonName, " ")
		lessonName = &tempLessonName
	} else {
		lessonName = nil
	}
	resp.CurriculumName = userExtra.CurriculumName

	if userExtra.Subscribe == nil {
		resp.Subscribe = 1
	} else if *userExtra.Subscribe == false {
		resp.Subscribe = 0
	}

	if userExtra.IsParent == true {
		resp.IsParent = 1
	}

	resp.LessonName = lessonName
}

func processReturnDataFromTableUserTeacher(resp *GetUserProfile, userTeacher *UserTeacher) {
	var overallRating float32
	if userTeacher.OverallRating != nil {
		overallRating = *userTeacher.OverallRating
	} else {
		overallRating = 0
	}
	resp.OverallRating = overallRating
	resp.LongAboutMe = userTeacher.GuruBio
	resp.ShortAboutMe = userTeacher.BioSingkat
	resp.RatingSummary = userTeacher.RatingSummary
	resp.TeacherTeachingPreference = userTeacher.TeacherTeachingPreference
	resp.IsVerified = userTeacher.IsVerified
}

func processReturnDataFromTableUserAddress(resp *GetUserProfile, userAddress *UserAddress, regionSvc region.Service) error {
	resp.DomicileKecamatan = userAddress.DomicileKecamatan
	resp.DomicilePostalCode = userAddress.DomicilePostalCode
	resp.DomicileAddress = userAddress.DomicileAddress
	resp.KtpKecamatan = userAddress.KtpKecamatan
	resp.KtpPostalCode = userAddress.KtpPostalCode
	resp.KtpAddress = userAddress.KtpAddress

	resp.KtpProvince = userAddress.KtpProvince
	resp.DomicileProvince = userAddress.DomicileProvince
	var provinceCode []int
	if userAddress.KtpProvince != nil {
		provinceCode = append(provinceCode, *userAddress.KtpProvince)
	}
	if userAddress.DomicileProvince != nil {
		provinceCode = append(provinceCode, *userAddress.DomicileProvince)
	}

	if len(provinceCode) > 0 {
		province, err := regionSvc.GetProvince(provinceCode)
		if err != nil {
			return err
		}
		if userAddress.KtpProvince != nil {
			var ktpProvinceName string
			ktpProvinceName = province.Data[strconv.Itoa(*userAddress.KtpProvince)].DisplayName
			resp.KtpProvinceName = &ktpProvinceName
		}
		if userAddress.DomicileProvince != nil {
			var domicileProvinceName string
			domicileProvinceName = province.Data[strconv.Itoa(*userAddress.DomicileProvince)].DisplayName
			resp.DomicileProvinceName = &domicileProvinceName
		}
	}

	resp.KtpCity = userAddress.KtpCity
	resp.DomicileCity = userAddress.DomicileCity
	var cityCode []int
	if userAddress.KtpCity != nil {
		cityCode = append(cityCode, *userAddress.KtpCity)
	}
	if userAddress.DomicileCity != nil {
		cityCode = append(cityCode, *userAddress.DomicileCity)
	}
	if len(cityCode) > 0 {
		city, err := regionSvc.GetCity(cityCode)
		if err != nil {
			return err
		}
		if userAddress.KtpCity != nil {
			var ktpCityName string
			ktpCityName = city.Data[strconv.Itoa(*userAddress.KtpCity)].DisplayName
			resp.KtpCityName = &ktpCityName
		}
		if userAddress.DomicileCity != nil {
			var domicileCityName string
			domicileCityName = city.Data[strconv.Itoa(*userAddress.DomicileCity)].DisplayName
			resp.DomicileCityName = &domicileCityName
		}
	}
	return nil
}

func processReturnDataFromTableUsers(resp *GetUserProfile, user *Users) {
	s3BaseUrl := "https://core-ruangguru.s3.amazonaws.com/"
	defaultProfPic := s3BaseUrl + "assets/avatar/avatar|2599.png"
	var profilePicFull string
	if user.ProfilePic == nil {
		profilePicFull = defaultProfPic
	} else {
		profilePicFull = s3BaseUrl + *user.ProfilePic
	}

	resp.Name = user.Name
	resp.Shortname = getShortName(user.Name)
	resp.Email = user.Email
	resp.Role = user.Role
	resp.CellphoneNumber = user.CellphoneNumber
	resp.Gender = user.Gender
	resp.BirthDate = user.BirthDate
	resp.BirthPlace = user.BirthPlace
	resp.ParentalCode = user.ParentalCode
	resp.ProfilePicFullDomain = profilePicFull
	resp.ProfilePic = user.ProfilePic
	resp.UniqueOrderCode = user.UniqueOrderCode
	resp.ActivationCode = user.ActivatedCode
	resp.IsActive = user.IsActive
	resp.CreatedAt = user.CreatedAt.Format("2006-01-02 15:04:05")
	resp.QiscusEmail = getQiscusEmail(user.ID)
	resp.ProfpicWithoutKey = getProfpicWithoutKey(user.ProfilePic)
	resp.GradeSerial = user.GradeSerial
	resp.CurriculumSerial = &user.CurriculumSerial
	resp.ReferralCode = user.ReferralCode
}

func (s *service) Register(ctx context.Context, req RegisterReq, userCode string) (RegisterResp, error) {
	r := regexp.MustCompile(" ")

	var user Users
	user.Password = encrypt.HashString(req.Password, req.Email)
	user.CellphoneNumber = r.ReplaceAllString(req.CellphoneNumber, "")
	user.Email = r.ReplaceAllString(req.Email, "")
	user.Name = req.Name

	if !validationutil.ValidateRegistrationRole(req.Role) {
		return RegisterResp{}, customfault.InvalidValueError{"role", "invalid regisration role"}
	}
	user.Role = req.Role
	user.Gender = "M"
	user.IsActive = "N"
	if req.LessonName != "" {
		gradeResponse, err := s.gradeSvc.GetGrade(req.LessonName)
		if err != nil {
			return RegisterResp{}, err
		}
		if gradeResponse.Message == "No record(s) found!" {
			err = &GradeNotFoundError{"lesson_name value is not allowed"}
			return RegisterResp{}, err
		}
		user.GradeSerial = gradeResponse.Data[0].Serial
	} else {
		user.GradeSerial = "umum"
	}
	user.CurriculumSerial = req.CurriculumSerial
	user.ActivatedCode = generateActivatedCode()
	parentalCode := serial.NewRandomLength("", 13)
	parentalCodeString := parentalCode.String()
	user.ParentalCode = parentalCodeString
	if userCode == "" {
		user.UniqueOrderCode = strings.ToUpper(serialutil.GenerateUniqueOrderCodeFromName(user.Name))
		user.ReferralCode = strings.ToLower(user.UniqueOrderCode)
	} else {
		user.UniqueOrderCode = userCode
		user.ReferralCode = strings.ToLower(serialutil.GenerateUniqueOrderCodeFromName(user.Name))
	}

	userExtra := &UserExtra{}

	err := s.CreateUser(&user, userExtra)
	if err != nil {
		if mysqlutil.IsDuplicatedEntryError(err) {
			user.UniqueOrderCode = strings.ToUpper(serialutil.GenerateUniqueOrderCodeFromName(user.Name))
			user.ReferralCode = strings.ToLower(user.UniqueOrderCode)
			err = s.CreateUser(&user, userExtra)
			if err != nil {
				return RegisterResp{}, err
			}
		} else {
			return RegisterResp{}, err
		}
	}
	if strings.ToLower(user.Role) == "teacher" || strings.ToLower(user.Role) == "student" {
		payload := emailClient.ActivationEmailReq{
			UserEmail:      user.Email,
			UserName:       user.Name,
			ActivationCode: user.ActivatedCode,
		}

		isStudent := true
		if user.Role == "teacher" {
			isStudent = false
		}

		errSendEmail := s.emailClientSvc.SendActivationEmail(payload, isStudent)
		if errSendEmail != nil {
			log.Printf("Error sending activation email : %v", errSendEmail)
			return RegisterResp{}, err
		}
	}

	registerResp := RegisterResp{
		UserCode: user.UniqueOrderCode,
		UserID:   user.ID,
	}
	return registerResp, nil
}

func generateActivatedCode() string {
	activatedCode := rand.Intn(12000-1000) + 1000
	return strconv.Itoa(activatedCode)
}

func (s *service) GetUserByEmail(ctx context.Context, email string) (users Users, err error) {
	users, err = s.repo.GetUserByEmail(ctx, email)
	return
}

func (s *service) GetUserByCellphoneNumber(ctx context.Context, cellphoneNumber string) (users Users, err error) {
	users, err = s.repo.GetUserByCellphoneNumber(ctx, cellphoneNumber)
	return
}

func (s *service) GetUsersByQuery(ctx context.Context, query GetUserQuery) (map[string]Users, error) {
	users, err := s.repo.GetUsersByQuery(ctx, query)
	if err != nil {
		return nil, err
	}
	usersMap := make(map[string]Users, len(users))
	for _, user := range users {
		usersMap[user.UniqueOrderCode] = user
	}
	return usersMap, err
}

func (s *service) GetUsersByUserIDs(ctx context.Context, query GetUserQuery) (map[int]Users, error) {
	users, err := s.repo.GetUsersByQuery(ctx, query)
	if err != nil {
		return nil, err
	}
	usersMap := make(map[int]Users, len(users))
	for _, user := range users {
		usersMap[user.ID] = user
	}
	return usersMap, err
}

func (s *service) CreateUserParent(ctx context.Context, userParent UserParent) error {
	return s.repo.CreateUserParent(ctx, userParent)
}

func (s *service) UpdateTnCAgreed(ctx context.Context, userID int, parentName string, parentCellphoneNumber string) error {
	return s.repo.UpdateTnCAgreed(ctx, userID, parentName, parentCellphoneNumber)
}

func (s *service) UpdateLastCheckParentalForm(ctx context.Context, userID int) error {
	return s.repo.UpdateLastCheckParentalForm(ctx, userID)
}

func (s *service) GetUserParentData(ctx context.Context, userID int) (*UserParent, error) {
	return s.repo.GetUserParentData(ctx, userID)
}

func (s *service) GetUser(email string) (*Users, error) {
	return s.repo.GetUser(email)
}

func (s *service) GetUserPercentage(ctx context.Context, userID int) (percentage int, err error) {
	user, err := s.repo.GetUserByUserID(userID)
	if err != nil {
		return 0, err
	}

	score := 0
	if user.Name != "" {
		score++
	}
	if user.Email != "" {
		score++
	}
	if user.Password != "" {
		score++
	}
	if user.CellphoneNumber != "" {
		score++
	}

	userExtra, err := s.repo.GetUserExtraData(ctx, userID)
	if err != nil {
		return score, err
	}
	if userExtra.LessonName != nil {
		score++
	}

	userAddress, err := s.repo.GetUserAddressData(ctx, userID)
	if err != nil {
		return score, err
	}

	if userAddress.DomicileCity != nil {
		score++
	}
	if userAddress.DomicileProvince != nil {
		score++
	}
	if userAddress.DomicileAddress != nil {
		score++
	}

	madatoryScore := 8
	percentage = 10
	if score > 0 {
		percentage = int(float32(score)/float32(madatoryScore)) * 100
	}

	return percentage, err
}

func (s *service) CreateUser(u *Users, uExtra *UserExtra) error {
	return s.repo.CreateUser(u, uExtra)
}

func (s *service) UpdateUserSubcribe(ctx context.Context, userID int, subscribe bool) error {
	userExtra, err := s.repo.GetUserExtraData(ctx, userID)
	if err != nil {
		return err
	}

	if userExtra == nil {
		uExtra := UserExtra{
			UserID:    userID,
			Subscribe: &subscribe,
			CreatedAt: time.Now(),
		}
		err = s.repo.CreateUserExtra(ctx, uExtra)
	} else {
		uExtra := UserExtra{
			UserID:    userID,
			Subscribe: &subscribe,
			UpdatedAt: time.Now(),
		}
		err = s.repo.UpdateUserSubscribe(ctx, uExtra)
	}

	return err
}

func (req LoginReq) ToUser() Users {
	return Users{
		Email:    req.Username,
		Password: req.Password,
	}
}

func (s *service) BuildTokenClaims(refreshToken string, u *Users, req LoginReq, isDebug bool) jwt.MapClaims {
	tokenExpiry := getTokenExpiry()
	claims := jwt.MapClaims{
		"rt":       refreshToken,
		"exp":      tokenExpiry,
		"uid":      u.ID,
		"uoc":      u.UniqueOrderCode,
		"r":        req.Role,
		"did":      req.DeviceID,
		"dn":       req.DeviceName,
		"is_debug": isDebug,
	}
	return claims
}

func (s *service) CreateToken(claims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(conf.SecretKeyToken))
	return ss, err
}

func (s *service) BuildRefreshTokenClaims(u *Users, req LoginReq, isDebug bool) jwt.MapClaims {
	refreshTokenExpiry := getRefreshTokenExpiry()
	claims := jwt.MapClaims{
		"cid":      req.ClientID,
		"exp":      refreshTokenExpiry,
		"uid":      u.ID,
		"uoc":      u.UniqueOrderCode,
		"r":        req.Role,
		"did":      req.DeviceID,
		"dn":       req.DeviceName,
		"is_debug": isDebug,
	}
	return claims
}

func (s *service) CreateRefreshToken(claims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(conf.SecretKeyRefreshToken))
	return ss, err
}

func (s *service) BuildFirebaseTokenClaims(u *Users) jwt.MapClaims {
	serviceAccountEmail := "firechat@firechat-6bf65.iam.gserviceaccount.com"
	claims := jwt.MapClaims{
		"iss": serviceAccountEmail,
		"sub": serviceAccountEmail,
		"aud": "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
		"iat": time.Now().Local().Unix(),
		"exp": getFirebaseTokenExpiry(),
		"uid": u.Email,
	}
	return claims
}

func (s *service) CreateFirebaseToken(claims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	var err error

	pemString := "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDCs1VAOMXTbDP6\nGrspF9Vb38l0bKckuPhTSbxaIj8k0bkW39spVk9goKyrB1CJ8oT7QSXeWW0sKkXT\nxGj2PC4RooUam2TUxHjuFlXA6aAJX75OLlKQytmIVj91wYw83DAJ5QXYwVMEiOB5\nkvS297EZyBe02L13YlmZW+tJO5cTcvMo2aTWCuHSvLNr5sZddbxX/lUd2Ll+5WXT\nFkcwDTuOjYKDKxQVXryIWIK5bGLT1UNbeoaYWZJsglJUvIBhUedZ56njiuqoQQMr\n2U74P1Btb9G1wbzOx0+AVHmH1sgdU31INJlOEr97PlxveJJ7gpXNnt62z4q8dgBT\n02H/AXOjAgMBAAECggEBAJlGZqGlUA+OCiklrBvAmIW4WjbeZoLhXxTqYm4usbkB\npY1dc4JKYTj3pJcLrhq7IndOJUqy6eAv80KwR9U5MSdO4yw8ExfxCCaezKmPAh7/\nEakojHVSTAgppdHFKeqriOHxOzYVk45pVxY+W4pC2lgqjocfbaCze0fg9gxz7g4v\nOqThEdir0Qh4lPFfhnLbv7KebAYsjKJzVoTjHh+rjXxA8Lg5lpzlc3pJ49/EGOr5\nYzYZn6EYuI9RxGJqbfh6UWTtrwMHn/u+s6sKdJJNVcOVbbImEyod7o+qUkZFESJh\nqzg0r56/8KixSFL4bED2rKl79NkvtOMcfECoEBcV7NkCgYEA8Ywfz1zaKaSAo/ty\nZK+jV7gD3vyw01aZs2NfdZf/ub9fFuEn9Z4q8Z+Zdsp4KwGhdRBnvt7QjYAD/xcm\n2FpGRmnnxFCKA9Rd2gqCEje1tUnq1OWplznTa8sejXuz94r64s29liOL9BywH/+1\nXIoAROAF553UbpHUYp8T6xBkXocCgYEAzlmjJj1gSSvXBNDoLHSig0cdZ66zlnla\niIWgtBObI5+LezPYpBAQTFZGEwkeSCFsOhGM+8gU8mferA7TDuDm2gIZq1PnmIlq\nYXRGCUnQdPPHq1hmM3vy+mQIyKk7wt/jDDnU259aHYVHVxX0ApDZ8L7fgdEsnqBF\nfw48BRC7TQUCgYBsTK9WggMu+JrU60B29GkAQ3k42YNOvl1bmQqL5wK5fZxc3cbU\n0X3fp/de05jdb3W8zwY1BiHrz6H7FZ5NEuy/VWydEJ9Q6D7MLDn1gRTvm5y2p+JJ\n85tfkUq5lOUz8BRc7HI8huORJ+uv7S6btyQ3vVnoZ1N/39KiBUEg+9CEsQKBgCwo\nwKJpVhaynBQ6xMTmvPSNHnzWxTX5iGdY+XgwDe5EsWvPP5h0m5hKijp+pA1ZobiX\nku0690ic8+ksTvyLIrwa4PVqQlDBb5Old2t7cXHlzaAcjdqMtwXgrISMuu5MTWGL\nasMJShaxNAAuAYcfCpTbtCcq5XqY3+Z93FhVKVAlAoGAI8igAWEL0Th7JYfdrw6K\nh3XBbl4KrSnuyqCOhWkLc2Rk4na4DaclpZOEDIHP1SaLGjRLd4w5qam6EBau5Lww\naA8ksS+Hzq3gbH+JyhjBibIUyPWY4LjhYRAnPmjKJOwme+MEqEOpuONIcYoOqiq3\nQRJ+hGu9WpJZJdd7o51JsXE=\n-----END PRIVATE KEY-----\n"

	block, _ := pem.Decode([]byte(pemString))
	if block == nil {
		return "", errors.New("ssh: no key found")
	}
	parseResult, _ := x509.ParsePKCS8PrivateKey(block.Bytes)
	key := parseResult.(*rsa.PrivateKey)
	ss, err := token.SignedString(key)
	return ss, err
}

func (s *service) BuildResponse(token string, refreshToken string, firebaseToken string, req LoginReq, u *Users) *ResponseData {
	expiresIn := getTokenExpiry()
	shortName := getShortName(u.Name)
	qiscusEmail := getQiscusEmail(u.ID)
	profPicWithoutKey := getProfpicWithoutKey(u.ProfilePic)
	returnData := ResponseData{
		Token:             token,
		RefreshToken:      refreshToken,
		ExpiresIn:         expiresIn,
		Role:              req.Role,
		Email:             u.Email,
		Name:              u.Name,
		ShortName:         shortName,
		UniqueOrderCode:   u.UniqueOrderCode,
		QiscusEmail:       qiscusEmail,
		ProfPicWithoutKey: profPicWithoutKey,
		DeviceID:          req.DeviceID,
		DeviceName:        req.DeviceName,
		FirebaseToken:     firebaseToken,
	}
	return &returnData
}

func (s *service) FillInRequest(a *LoginReq) {
	if a.DeviceID == "" {
		a.DeviceID = "None"
	}
	if a.DeviceName == "" {
		a.DeviceName = "None"
	}
	if a.UseFirebaseToken == "" {
		a.UseFirebaseToken = "N"
	}
}

func getTokenExpiry() int64 {
	exp := time.Now().Local().Add(time.Hour * time.Duration(conf.TokenExpiryInHours))
	return exp.Unix()
}

func getRefreshTokenExpiry() int64 {
	exp := time.Now().Local().Add(time.Hour * time.Duration(conf.RefreshTokenExpiryInHours))
	return exp.Unix()
}

func getFirebaseTokenExpiry() int64 {
	exp := time.Now().Local().Add(time.Hour * time.Duration(conf.FirebaseTokenExpiryInHours))
	return exp.Unix()
}

func getShortName(name string) string {
	splitName := strings.Split(name, " ")
	firstName := splitName[0]
	lastNameFirstCharacter := ""
	if len(splitName) > 1 {
		lastName := splitName[1]
		if len(lastName) > 0 {
			lastNameFirstCharacter = lastName[:1]
		}
	}
	return strings.Join([]string{firstName, lastNameFirstCharacter}, " ")
}

func getQiscusEmail(userID int) string {
	hasher := md5.New()
	hasher.Write([]byte(strconv.Itoa(userID)))
	md5UserID := hex.EncodeToString(hasher.Sum(nil))
	return strings.Join([]string{"user_id_", strconv.Itoa(userID), "_", md5UserID, "@ruangguru.com"}, "")
}

func getProfpicWithoutKey(profilePic *string) string {
	if profilePic == nil {
		return strings.Join([]string{conf.KongGWURL, "/aws-image?key=None"}, "")
	}
	return strings.Join([]string{conf.KongGWURL, "/aws-image?key=", *profilePic}, "")
}

func (s *service) UpdateUsersByAdmin(ctx context.Context, users []UpdateUsersByAdminQuery) error {
	if conf.IsUpdateUsersByAdminInTransactionMode {
		errUpdate := s.repo.UpdateUsersByAdminInTransaction(ctx, users)
		if errUpdate != nil {
			return errUpdate
		}
	} else {
		for _, u := range users {
			errUpdate := s.repo.UpdateUsersByAdmin(ctx, &u)
			if errUpdate != nil {
				return errUpdate
			}
		}
	}
	return nil
}

func (s *service) GetUsersByEmailOrCellphoneNumber(email string, cellphoneNumber string) (users Users, err error) {
	users, err = s.repo.GetUsersByEmailOrCellphoneNumber(email, cellphoneNumber)
	return
}

func (s *service) CheckUserParent(ctx context.Context, userID int) (bool, error) {
	_, err := s.repo.GetUserParentData(ctx, userID)
	if fmt.Sprintf("%s", err) == "User ID not found" {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	return true, err
}

func (s *service) CheckUserActivation(userID int) (UserActivationStatus, error) {
	u, err := s.repo.GetUserByUserID(userID)
	if err != nil {
		return UserActivationStatus{}, err
	}
	return UserActivationStatus{
		Email:    u.Email,
		IsActive: u.IsActive,
	}, err
}

func (s *service) CreateUserRukerCMSBulk(ctx context.Context, payload []CreateUserRukerCMSBulkReq) ([]CreateUserRukerCMSBulkResp, error) {
	ret := []CreateUserRukerCMSBulkResp{}

	// Collect Email into arrays
	emailArrayQuery := []string{}
	for _, newUserPayload := range payload {
		emailArrayQuery = append(emailArrayQuery, newUserPayload.Email)
	}

	// Get already registered users by email array
	existingUsers, err := s.repo.GetUsersByQuery(ctx, GetUserQuery{
		Emails: emailArrayQuery,
	})
	if err != nil {
		return nil, err
	}

	// Create map of registered users by email
	existingUserMapByEmail := map[string]Users{}
	for _, existingUser := range existingUsers {
		existingUserMapByEmail[existingUser.Email] = existingUser
	}

	for _, newUserPayload := range payload {
		// Check if email already registered
		existingUser, ok := existingUserMapByEmail[newUserPayload.Email]

		if ok {
			// User Email already registered, mark as Existing
			ret = append(ret, CreateUserRukerCMSBulkResp{
				UserSerial: existingUser.UniqueOrderCode,
				Email:      existingUser.Email,
				Status:     "Existing",
			})

			continue
		}

		userSerialString := serialutil.GenerateUniqueOrderCodeFromName(newUserPayload.Name)

		// Loop until unique ??

		parentalCode := serial.NewRandomLength("", 13)
		parentalCodeString := parentalCode.String()

		newUser := &Users{
			UniqueOrderCode: userSerialString,
			ReferralCode:    userSerialString,
			GradeSerial:     "umum", // default value for ruker CMS bulk, no impact in ruker system
			ParentalCode:    parentalCodeString,
			Name:            newUserPayload.Name,
			CellphoneNumber: newUserPayload.Phone,
			Password:        newUserPayload.HashedPassword,
			Email:           newUserPayload.Email,
			Role:            newUserPayload.Role,
		}
		newUserExtra := &UserExtra{}

		err := s.repo.CreateUser(newUser, newUserExtra)
		if err != nil {
			ret = append(ret, CreateUserRukerCMSBulkResp{
				Email:    newUserPayload.Email,
				Status:   "Failed",
				ErrorMsg: err.Error(),
			})
		} else {
			ret = append(ret, CreateUserRukerCMSBulkResp{
				UserSerial: userSerialString,
				Email:      newUserPayload.Email,
				Status:     "New",
			})
		}
	}

	return ret, nil
}

type RefreshTokenExpiredError struct {
	Msg string
}

func (e *RefreshTokenExpiredError) Error() string {
	return fmt.Sprintf("%v", e.Msg)
}

func (s *service) RefreshToken(payload RefreshTokenReq) (RefreshTokenResponse, error) {
	refreshToken := payload.RefreshToken
	parsedToken, err := jwt.Parse(refreshToken, func(refreshToken *jwt.Token) (interface{}, error) {
		return []byte(conf.SecretKeyRefreshToken), nil
	})

	if err != nil {
		vErr := err.(*jwt.ValidationError)
		switch vErr.Errors {
		case jwt.ValidationErrorExpired:
			return RefreshTokenResponse{}, &RefreshTokenExpiredError{"refresh token invalid"}
		default:
			return RefreshTokenResponse{}, err
		}
	}

	if claims, ok := parsedToken.Claims.(jwt.MapClaims); ok {
		timeNow := float64(time.Now().Unix())
		tokenExpiry := claims["exp"].(float64)

		if timeNow < tokenExpiry {
			anon := false
			if val, ok := claims["anon"]; ok {
				anon = val.(bool)
			}
			if anon {
				userToken, err := s.authSvc.AuthTokenUser(claims["r"].(string), conf.ClientIDAndroid, conf.ClientSecretAndroid)
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				token := userToken.Data[0].Token
				refreshToken := userToken.Data[0].RefreshToken
				role := userToken.Data[0].Role

				userData, err := s.authSvc.ValidateToken(token)
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				expiresIn := userData.Data[0].ExpiresIn
				userCode := userData.Data[0].UserCode

				return RefreshTokenResponse{
					Token:           token,
					RefreshToken:    refreshToken,
					ExpiresIn:       expiresIn,
					Role:            role,
					UniqueOrderCode: userCode,
					Anon:            true,
				}, nil
			} else {
				userData, err := s.repo.GetUserByUOC(claims["uoc"].(string))
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				req := LoginReq{
					ClientID:   payload.ClientID,
					Role:       claims["r"].(string),
					DeviceID:   payload.DeviceID,
					DeviceName: payload.DeviceName,
				}
				isDebug := false
				if val, ok := claims["is_debug"]; ok {
					isDebug = val.(bool)
				}
				refreshTokenClaims := s.BuildRefreshTokenClaims(userData, req, isDebug)
				refreshToken, err := s.CreateRefreshToken(refreshTokenClaims)
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				tokenClaims := s.BuildTokenClaims(refreshToken, userData, req, isDebug)
				token, err := s.CreateToken(tokenClaims)
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				firebaseTokenClaims := s.BuildFirebaseTokenClaims(userData)
				firebaseToken, err := s.CreateFirebaseToken(firebaseTokenClaims)
				if err != nil {
					return RefreshTokenResponse{}, err
				}
				expiresIn := getTokenExpiry()
				shortName := getShortName(userData.Name)
				qiscusEmail := getQiscusEmail(userData.ID)
				profPicWithoutKey := getProfpicWithoutKey(userData.ProfilePic)
				return RefreshTokenResponse{
					Token:             token,
					RefreshToken:      refreshToken,
					ExpiresIn:         expiresIn,
					Role:              claims["r"].(string),
					Email:             userData.Email,
					Name:              userData.Name,
					Shortname:         shortName,
					UniqueOrderCode:   userData.UniqueOrderCode,
					QiscusEmail:       qiscusEmail,
					ProfpicWithoutKey: profPicWithoutKey,
					FirebaseToken:     firebaseToken,
					Anon:              false,
				}, nil
			}
		}
	}

	return RefreshTokenResponse{}, fmt.Errorf("token invalid or expired")
}

func (s *service) ResendActivation(email string) error {
	u, err := s.repo.GetUser(email)
	if err != nil {
		return err
	}

	if u.IsActive == "Y" {
		return &ErrUserHasBeenActivated{}
	}

	payload := emailClient.ActivationEmailReq{
		UserEmail:      u.Email,
		UserName:       u.Name,
		ActivationCode: u.ActivatedCode,
	}

	isStudent := true
	if u.Role == "teacher" {
		isStudent = false
	}

	errSendEmail := s.emailClientSvc.SendActivationEmail(payload, isStudent)
	if errSendEmail != nil {
		log.Printf("Error sending activation email : %v", errSendEmail)
	}
	return errSendEmail
}

func (s *service) ChangePassword(ctx context.Context, userID int, payload ChangePasswordReq) (err error) {
	u, err := s.repo.GetUserByUserID(userID)
	if err != nil {
		return err
	}

	oldPassword := encrypt.HashString(payload.OldPassword, u.Email)
	if oldPassword != u.Password {
		err := fault.ErrorDictionaryWithContext(ctx, fault.WrongPasswordErrorCode, "")
		return err
	}

	newPassword := encrypt.HashString(payload.NewPasswordConfirmation, u.Email)
	err = s.repo.UpdateUserPassword(ctx, userID, newPassword)
	return err
}

func (s *service) GetForgotPassword(ctx context.Context, email string, isRuker bool) (key string, err error) {
	u, err := s.repo.GetUser(email)
	if err != nil {
		return "", err
	}

	keyLostPassword := ""
	if isRuker {
		password := serial.GenerateWithLength(10)
		hashedPassword := encrypt.HashString(password, u.Email)
		err = s.repo.UpdateUserPassword(ctx, u.ID, hashedPassword)
		if err != nil {
			return "", err
		}

		err = s.emailClientSvc.SendResetPassword(u.Name, u.Email, password)
		if err != nil {
			log.Printf("Error sending reset password email : %v", err)
		}
	} else {
		keyLostPassword = serial.GenerateWithLength(40)
		uExtraData, err := s.repo.GetUserExtraData(ctx, u.ID)
		if err != nil {
			return "", err
		}

		if uExtraData == nil {
			uExtra := UserExtra{
				UserID:          u.ID,
				KeyLostPassword: &keyLostPassword,
				CreatedAt:       time.Now(),
			}
			err = s.repo.CreateUserExtra(ctx, uExtra)
			if err != nil {
				return "", err
			}
		} else {
			uExtra := UserExtra{
				UserID:          u.ID,
				KeyLostPassword: &keyLostPassword,
				UpdatedAt:       time.Now(),
			}
			err = s.repo.UpdateUserExtra(ctx, uExtra)
			if err != nil {
				return "", err
			}
		}

		err = s.emailClientSvc.SendForgotPassword(u.Name, u.Email, keyLostPassword)
		if err != nil {
			log.Printf("Error sending forgot password email : %v", err)
		}
	}
	return keyLostPassword, err
}

func (s *service) SetForgotPassword(ctx context.Context, payload ForgotPasswordReq) (err error) {
	u, err := s.repo.GetUser(payload.Email)
	if err != nil {
		return
	}

	uExtraData, err := s.repo.GetUserExtraData(ctx, u.ID)
	if err != nil {
		return
	}

	if *uExtraData.KeyLostPassword != payload.KeyLostPwd {
		return fmt.Errorf("key invalid")
	}

	hashedPassword := encrypt.HashString(payload.Password, u.Email)
	err = s.repo.UpdateUserPassword(ctx, u.ID, hashedPassword)
	if err != nil {
		return
	}
	keyLostPassword := fmt.Sprintf("used at %v", time.Now().Format(time.RFC3339))
	uExtra := UserExtra{
		UserID:          u.ID,
		KeyLostPassword: &keyLostPassword,
		UpdatedAt:       time.Now(),
	}
	err = s.repo.UpdateUserExtra(ctx, uExtra)
	if err != nil {
		return
	}
	return
}

func (s *service) ChangeUserEmail(ctx context.Context, payload ChangeUserEmailReq) error {
	// Validate password
	if len(payload.Password) < 6 {
		err := fault.ErrorDictionaryWithContext(ctx, fault.PasswordNotEnoughCharacterErrorCode, "")
		return err
	}

	// Validate new email : must not be already registered
	userFromNewEmail, err := s.GetUserByEmail(ctx, payload.Email)
	if err != nil {
		return err
	}
	if userFromNewEmail.Email == payload.Email {
		errMsg := fmt.Sprintf("User with email %s already registered", payload.Email)
		err := fault.ErrorDictionaryWithContext(ctx, fault.EmailHadBeenRegisteredErrorCode, errMsg)
		return err
	}

	// Validate existing user : must exist
	oldUserMap, err := s.GetUsersByQuery(ctx, GetUserQuery{
		UserSerials: []string{payload.UserSerial},
	})
	if err != nil {
		return err
	}
	oldUser, ok := oldUserMap[payload.UserSerial]
	if !ok || oldUser.UniqueOrderCode != payload.UserSerial {
		errMsg := fmt.Sprintf("User with serial %s not found", payload.UserSerial)
		err := fault.ErrorDictionaryWithContext(ctx, fault.GeneralRepositoryErrorCode, errMsg)
		return err
	}

	// Set new value
	oldUser.Password = encrypt.HashString(payload.Password, payload.Email)
	oldUser.Email = payload.Email

	// Send to repo & return
	return s.repo.UpdateSingleUser(ctx, &oldUser)
}
