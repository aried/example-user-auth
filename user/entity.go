package user

import (
	"time"

	"github.com/go-sql-driver/mysql"
)

type GetAllUsersByQueryResponse struct {
	ID              string `gorm:"primary_key" json:"id"`
	Name            string `json:"name"`
	Email           string `json:"email"`
	Role            string `json:"role"`
	CellphoneNumber string `json:"cellphone_number"`
	IsActive        string `json:"is_active"`
	UniqueOrderCode string `json:"unique_order_code"`
	CreatedAt       string `json:"created_at"`
	Gender          string `json:"gender"`
	ProfilePic      string `json:"profile_pic"`
	GradeSerial     string `json:"grade_serial"`
	DomicileCity    *int   `json:"domicile_city"`
}

type GetAllUsersByQueryResult struct {
	ID              int       `gorm:"primary_key" json:"id"`
	Name            string    `json:"name"`
	Email           string    `json:"email"`
	Role            string    `json:"role"`
	CellphoneNumber string    `json:"cellphone_number"`
	IsActive        string    `json:"is_active"`
	UniqueOrderCode string    `json:"unique_order_code"`
	CreatedAt       time.Time `json:"created_at"`
	Gender          string    `json:"gender"`
	ProfilePic      *string   `json:"profile_pic"`
	GradeSerial     string    `json:"grade_serial"`
	DomicileCity    *int      `json:"domicile_city"`
}

type GetAllUsersQuery struct {
	UserID          string `form:"user_id"`
	Name            string `form:"name"`
	Email           string `form:"email"`
	Role            string `form:"role"`
	CellphoneNumber string `form:"cellphone_number"`
	IsActive        string `form:"is_active"`
	UserSerial      string `form:"userSerial"`
	CreatedAt       string `form:"created_at"`
	Gender          string `form:"gender"`
	GradeSerial     string `form:"grade_serial"`
	Page            int    `form:"page"`
	PageSize        int    `form:"page_size"`
}

type UpdateFormRequest struct {
	DeletedAt              *bool    `form:"deleted_at"`
	CellphoneNumber        string   `form:"cellphone_number"`
	Name                   string   `form:"name"`
	Gender                 string   `form:"gender"`
	BirthDate              string   `form:"birth_date"`
	BirthPlace             *string  `form:"birth_place"`
	LessonName             *string  `form:"lesson_name"`
	CurriculumSerial       *string  `form:"curriculum_serial"`
	PendidikanId           *int     `form:"pendidikan_id"`
	GuruPendidikanInstansi *string  `form:"guru_pendidikan_instansi"`
	GuruPendidikanVerified *bool    `form:"guru_pendidikan_verified"`
	GuruTwitter            *string  `form:"guru_twitter"`
	GuruReferral           *int     `form:"guru_referral"`
	KategoriId             *int     `form:"kategori_id"`
	GuruRating             *float64 `form:"guru_rating"`
	GuruIspro              *string  `form:"guru_ispro"`
	LongAboutMe            *string  `form:"long_about_me"`
	GuruReview             *string  `form:"guru_review"`
	GuruKualifikasi        *string  `form:"guru_kualifikasi"`
	GuruPengalaman         *string  `form:"guru_pengalaman"`
	GuruMetode             *string  `form:"guru_metode"`
	GuruBlocked            *bool    `form:"guru_blocked"`
	GuruLastLogin          string   `form:"guru_last_login"`
	JenjangPendidikan      *string  `form:"jenjang_pendidikan"`
	TeachOnline            *bool    `form:"teach_online"`
	IsThebest              *bool    `form:"is_thebest"`
	RatingSummary          string   `form:"rating_summary"`
	OverallRating          *float32 `form:"overall_rating"`
	IsVerified             string   `form:"is_verified"`
	ShortAboutMe           *string  `form:"short_about_me"`
	TagLine                *string  `form:"tag_line"`
	ParentName             string   `form:"parent_name"`
	ParentCellphoneNumber  string   `form:"parent_cellphone_number"`
	Degree                 *string  `form:"degree"`
	SchoolName             *string  `form:"school_name"`
	Nisn                   *string  `form:"nisn"`
	Npsn                   *string  `form:"npsn"`
	LastUrl                *string  `form:"last_url"`
	Source                 *string  `form:"source"`
	AdminCode              *string  `form:"admin_code"`
	IdAdminProfile         *int     `form:"id_admin_profile"`
	IsParent               string   `form:"is_parent"`
	Subscribe              *bool    `form:"subscribe"`
	FacebookID             *string  `form:"facebook_id"`
	ReferralCode           *string  `form:"referral_code"`
	CellphoneNumber_2      *string  `form:"cellphone_number_2"`
	OfficePhoneNumber      *string  `form:"office_phone_number"`
	CallStatus             *bool    `form:"call_status"`
	CallProgress           *bool    `form:"call_progress"`
	MuridHandleBy          *int     `form:"murid_handle_by"`
	CampaignCode           *string  `form:"campaign_code"`
	Preference             *string  `form:"preference"`
	SourceInfoId           *string  `form:"source_info_id"`
	DomicileAddress        *string  `form:"domicile_address"`
	DomicileCity           *int     `form:"domicile_city"`
	DomicileProvince       *int     `form:"domicile_province"`
	DomicileKecamatan      *int     `form:"domicile_kecamatan"`
	DomicilePostalCode     *string  `form:"domicile_postal_code"`
	KtpAddress             string   `form:"ktp_address"`
	KtpCity                *int     `form:"ktp_city"`
	KtpProvince            *int     `form:"ktp_province"`
	KtpKecamatan           *int     `form:"ktp_kecamatan"`
	KtpPostalCode          *string  `form:"ktp_postal_code"`
	IdIdentityType         *int     `form:"id_identity_type"`
	NikNo                  *string  `form:"nik_no"`
	NikVerified            *bool    `form:"nik_verified"`
	NpwpNo                 *string  `form:"npwp_no"`
	TaxMonth               *int     `form:"tax_month"`
	TaxYear                *int     `form:"tax_year"`
	NpwpImageVerified      *bool    `form:"npwp_image_verified"`
	TaxNoteVerified        *bool    `form:"tax_note_verified"`
	PernyataanPenghasilan  *string  `form:"pernyataan_penghasilan"`
	BankId                 *int     `form:"bank_id"`
	BankBranch             *string  `form:"bank_branch"`
	GuruBankRekening       *string  `form:"guru_bank_rekening"`
	GuruBankPemilik        *string  `form:"guru_bank_pemilik"`
}

type GetUserProfileFromUsersTableResp struct {
	Name                 string     `json:"name"`
	Shortname            string     `json:"shortname"`
	Email                string     `json:"email"`
	Role                 string     `json:"role"`
	CellphoneNumber      string     `json:"cellphone_number"`
	Gender               string     `json:"gender"`
	BirthDate            *time.Time `json:"birth_date"`
	BirthPlace           *string    `json:"birth_place"`
	ParentalCode         string     `json:"parental_code"`
	ProfilePicFullDomain string     `json:"profile_pic_full_domain"`
	ProfilePic           *string    `json:"profile_pic"`
	UniqueOrderCode      string     `json:"unique_order_code"`
	ActivationCode       string     `json:"activation_code"`
	IsActive             string     `json:"is_active"`
	CreatedAt            string     `json:"created_at"`
	QiscusEmail          string     `json:"qiscus_email"`
	ProfpicWithoutKey    string     `json:"profpic_without_key"`
	GradeSerial          string     `json:"grade_serial"`
	ReferralCode         string     `json:"referral_code"`
}

type GetUserProfile struct {
	Name                      string     `json:"name"`
	Shortname                 string     `json:"shortname"`
	Email                     string     `json:"email"`
	Role                      string     `json:"role"`
	CellphoneNumber           string     `json:"cellphone_number"`
	Gender                    string     `json:"gender"`
	BirthDate                 *time.Time `json:"birth_date"`
	BirthPlace                *string    `json:"birth_place"`
	ParentalCode              string     `json:"parental_code"`
	ProfilePicFullDomain      string     `json:"profile_pic_full_domain"`
	ProfilePic                *string    `json:"profile_pic"`
	UniqueOrderCode           string     `json:"unique_order_code"`
	ActivationCode            string     `json:"activation_code"`
	IsActive                  string     `json:"is_active"`
	CreatedAt                 string     `json:"created_at"`
	QiscusEmail               string     `json:"qiscus_email"`
	ProfpicWithoutKey         string     `json:"profpic_without_key"`
	GradeSerial               string     `json:"grade_serial"`
	CurriculumSerial          *string    `json:"curriculum_serial"`
	ReferralCode              string     `json:"referral_code"`
	OverallRating             float32    `json:"overall_rating"`
	LongAboutMe               *string    `json:"long_about_me"`
	ShortAboutMe              *string    `json:"short_about_me"`
	RatingSummary             float32    `json:"rating_summary"`
	TeacherTeachingPreference *string    `json:"teacher_teaching_preference"`
	IsVerified                bool       `json:"is_verified"`
	OfficePhoneNumber         *string    `json:"office_phone_number"`
	CellphoneNumber2          *string    `json:"cellphone_number_2"`
	CreditBalance             float32    `json:"credit_balance"`
	Subscribe                 int        `json:"subscribe"`
	Degree                    *string    `json:"degree"`
	LessonName                *string    `json:"lesson_name"`
	CurriculumName            *string    `json:"curriculum_name"`
	SchoolName                *string    `json:"school_name"`
	IsParent                  int        `json:"is_parent"`
	LastUrl                   *string    `json:"last_url"`
	DomicileProvince          *int       `json:"domicile_province"`
	DomicileProvinceName      *string    `json:"domicile_province_name"`
	DomicileCity              *int       `json:"domicile_city"`
	DomicileCityName          *string    `json:"domicile_city_name"`
	DomicileKecamatan         *int       `json:"domicile_kecamatan"`
	DomicilePostalCode        *string    `json:"domicile_postal_code"`
	DomicileAddress           *string    `json:"domicile_address"`
	KtpProvince               *int       `json:"ktp_province"`
	KtpProvinceName           *string    `json:"ktp_province_name"`
	KtpCity                   *int       `json:"ktp_city"`
	KtpCityName               *string    `json:"ktp_city_name"`
	KtpKecamatan              *int       `json:"ktp_kecamatan"`
	KtpPostalCode             *string    `json:"ktp_postal_code"`
	KtpAddress                string     `json:"ktp_address"`
	NikNo                     *string    `json:"nik_no"`
	NikImage                  *string    `json:"nik_image"`
	NpwpNo                    *string    `json:"npwp_no"`
	NpwpImage                 *string    `json:"npwp_image"`
	BankId                    *int       `json:"bank_id"`
	BankName                  *string    `json:"bank_name"`
	BankAccountNo             *string    `json:"bank_account_no"`
	BankAccountName           *string    `json:"bank_account_name"`
	ShowParentalForm          bool       `json:"showParentalForm"`
	ParentName                string     `json:"parent_name"`
	ParentCellphoneNumber     string     `json:"parent_cellphone_number"`
}

type RegisterReq struct {
	Email                string `binding:"required" json:"email"`
	Name                 string `binding:"required" json:"name"`
	Password             string `binding:"required" json:"password"`
	PasswordConfirmation string `json:"password_confirmation"`
	Role                 string `binding:"required" json:"role"`
	LessonName           string `json:"lesson_name"`
	CurriculumSerial     string `json:"curriculum_serial"`
	CellphoneNumber      string `json:"cellphone_number"`
	Token                string `json:"token"`
}

type RegisterResp struct {
	UserCode string `json:"user_code"`
	UserID   int    `json:"user_id"`
}

type LoginReq struct {
	Username         string `form:"username" binding:"required" json:"username"`
	Password         string `form:"password" binding:"required" json:"password"`
	LoginType        string `form:"type" binding:"required" json:"type"`
	Role             string `form:"role" binding:"required" json:"role"`
	ClientID         string `form:"client_id" binding:"required" json:"client_id"`
	ClientSecret     string `form:"client_secret" binding:"required" json:"client_secret"`
	DeviceID         string `form:"device_id" binding:"omitempty" json:"device_id"`
	DeviceName       string `form:"device_name" binding:"omitempty" json:"device_name"`
	UseFirebaseToken string `form:"use_firebase_token" binding:"omitempty" json:"use_firebase_token"`
}

type Users struct {
	ID               int        `gorm:"primary_key" json:"id"`
	Name             string     `json:"name"`
	Email            string     `json:"email"`
	Password         string     `json:"-"`
	Role             string     `json:"role"`
	CellphoneNumber  string     `json:"cellphone_number"`
	IsActive         string     `json:"is_active"`
	ActivatedAt      time.Time  `json:"activated_at"`
	ActivatedCode    string     `json:"activated_code"`
	UniqueOrderCode  string     `json:"unique_order_code"`
	Gender           string     `json:"gender"`
	BirthPlace       *string    `json:"birth_place"`
	BirthDate        *time.Time `json:"birth_date"`
	DeletedAt        *time.Time `json:"deleted_at"`
	DeletedBy        *int       `json:"deleted_by"`
	CreatedAt        time.Time  `json:"created_at"`
	UpdatedAt        time.Time  `json:"updated_at"`
	UpdatedBy        *string    `json:"updated_by"`
	ProfilePic       *string    `json:"profile_pic"`
	ParentalCode     string     `json:"parental_code"`
	GradeSerial      string     `json:"grade_serial"`
	CurriculumSerial string     `json:"curriculum_serial"`
	ReferralCode     string     `json:"referral_code"`
}

type UserAddress struct {
	ID                 int        `gorm:"primary_key" json:"id"`
	UserID             int        `json:"user_id"`
	Province           *string    `json:"province"`
	City               *string    `json:"city"`
	KtpAddress         string     `json:"ktp_address"`
	DeletedAt          *time.Time `json:"deleted_at"`
	DeletedBy          *int       `json:"deleted_by"`
	CreatedAt          time.Time  `json:"created_at"`
	UpdatedAt          time.Time  `json:"updated_at"`
	UpdatedBy          *int       `json:"updated_by"`
	DomicileAddress    *string    `json:"domicile_address"`
	Kecamatan          *string    `json:"kecamatan"`
	KecamatanKtp       *string    `json:"kecamatan_ktp"`
	DomicilePostalCode *string    `json:"domicile_postal_code"`
	DomicileKecamatan  *int       `json:"domicile_kecamatan"`
	DomicileProvince   *int       `json:"domicile_province"`
	DomicileCity       *int       `json:"domicile_city"`
	KtpPostalCode      *string    `json:"ktp_postal_code"`
	KtpKecamatan       *int       `json:"ktp_kecamatan"`
	KtpProvince        *int       `json:"ktp_province"`
	KtpCity            *int       `json:"ktp_city"`
}

type UserExtra struct {
	ID                int        `gorm:"primary_key" json:"id"`
	UserID            int        `json:"user_id"`
	CreditBalance     float32    `json:"credit_balance"`
	FacebookID        *string    `json:"facebook_id"`
	DeletedAt         *time.Time `json:"deleted_at"`
	DeletedBy         *int       `json:"deleted_by"`
	CreatedAt         time.Time  `json:"created_at"`
	UpdatedAt         time.Time  `json:"updated_at"`
	UpdatedBy         *int       `json:"updated_by"`
	ReferralCode      *string    `json:"referral_code"`
	CellphoneNumber_2 *string    `json:"cellphone_number_2"`
	OfficePhoneNumber *string    `json:"office_phone_number"`
	CallStatus        *bool      `json:"call_status"`
	CallProgress      *bool      `json:"call_progress"`
	MuridHandleBy     *int       `json:"murid_handle_by"`
	CampaignCode      *string    `json:"campaign_code"`
	Preference        *string    `json:"preference"`
	SourceInfoId      *string    `json:"source_info_id"`
	Subscribe         *bool      `json:"subscribe"`
	IsParent          bool       `json:"is_parent"`
	KeyLostPassword   *string    `json:"key_lost_password"`
	IdAdminProfile    *int       `json:"id_admin_profile"`
	AdminCode         *string    `json:"admin_code"`
	Source            *string    `json:"source"`
	Nisn              *string    `json:"nisn"`
	Npsn              *string    `json:"npsn"`
	LastUrl           *string    `json:"last_url"`
	SchoolName        *string    `json:"school_name"`
	LessonName        *string    `json:"lesson_name"`
	CurriculumName    *string    `json:"curriculum_name"`
	LineMid           *string    `json:"line_mid"`
	Degree            *string    `json:"degree"`
	GoogleID          *string    `json:"google_id"`
}

type UserTeacher struct {
	ID                        int        `gorm:"primary_key" json:"id"`
	UserID                    int        `json:"user_id"`
	OverallRating             *float32   `json:"overall_rating"`
	DeletedAt                 *time.Time `json:"deleted_at"`
	DeletedBy                 *int       `json:"deleted_by"`
	CreatedAt                 time.Time  `json:"created_at"`
	UpdatedAt                 time.Time  `json:"updated_at"`
	UpdatedBy                 *int       `json:"updated_by"`
	PendidikanId              *int       `json:"pendidikan_id"`
	GuruPendidikanInstansi    *string    `json:"guru_pendidikan_instansi"`
	GuruPendidikanVerified    *bool      `json:"guru_pendidikan_verified"`
	GuruTwitter               *string    `json:"guru_twitter"`
	GuruReferral              *int       `json:"guru_referral"`
	KategoriId                *int       `json:"kategori_id"`
	GuruRating                *float64   `json:"guru_rating"`
	GuruIspro                 *string    `json:"guru_ispro"`
	GuruBio                   *string    `json:"guru_bio"`
	GuruReview                *string    `json:"guru_review"`
	GuruKualifikasi           *string    `json:"guru_kualifikasi"`
	GuruPengalaman            *string    `json:"guru_pengalaman"`
	GuruMetode                *string    `json:"guru_metode"`
	GuruBlocked               *bool      `json:"guru_blocked"`
	GuruLastLogin             *time.Time `json:"guru_last_login"`
	BioSingkat                *string    `json:"bio_singkat"`
	JenjangPendidikan         *string    `json:"jenjang_pendidikan"`
	IsVerified                bool       `json:"is_verified"`
	RatingSummary             float32    `json:"rating_summary"`
	IsThebest                 *bool      `json:"is_thebest"`
	TeachOnline               *bool      `json:"teach_online"`
	TeacherTeachingPreference *string    `json:"teacher_teaching_preference"`
	TagLine                   *string    `json:"tag_line"`
}

type UserAdministrative struct {
	ID                        int            `gorm:"primary_key" json:"id"`
	UserID                    int            `json:"user_id"`
	DeletedAt                 *time.Time     `json:"deleted_at"`
	DeletedBy                 *int           `json:"deleted_by"`
	CreatedAt                 time.Time      `json:"created_at"`
	UpdatedAt                 time.Time      `json:"updated_at"`
	UpdatedBy                 *int           `json:"updated_by"`
	IdIdentityType            *int           `json:"id_identity_type"`
	NikNo                     *string        `json:"nik_no"`
	NikImage                  *string        `json:"nik_image"`
	NikImageModified          mysql.NullTime `json:"nik_image_modified"`
	NikVerified               *bool          `json:"nik_verified"`
	NpwpNo                    *string        `json:"npwp_no"`
	TaxMonth                  *int           `json:"tax_month"`
	TaxYear                   *int           `json:"tax_year"`
	NpwpImage                 *string        `json:"npwp_image"`
	NpwpImageVerified         *bool          `json:"npwp_image_verified"`
	NpwpUpdate                mysql.NullTime `json:"npwp_update"`
	TaxNoteImage              *string        `json:"tax_note_image"`
	TaxNoteVerified           *bool          `json:"tax_note_verified"`
	TaxNoteUpdated            mysql.NullTime `json:"tax_note_updated"`
	BankId                    *int           `json:"bank_id"`
	BankBranch                *string        `json:"bank_branch"`
	GuruBankRekening          *string        `json:"guru_bank_rekening"`
	GuruBankPemilik           *string        `json:"guru_bank_pemilik"`
	PernyataanPenghasilan     *string        `json:"pernyataan_penghasilan"`
	PernyataanPenghasilanFile *string        `json:"pernyataan_penghasilan_file"`
}

type UserParent struct {
	ID                    int        `gorm:"primary_key" json:"id"`
	UserID                int        `json:"user_id"`
	ParentName            string     `json:"parent_name"`
	ParentCellphoneNumber string     `json:"parent_cellphone_number"`
	CreatedAt             time.Time  `json:"created_at"`
	DeletedAt             *time.Time `json:"deleted_at"`
	TncApproved           *bool      `json:"tnc_approved"`
	LastCheckParentalForm *time.Time `json:"last_check_parental_form"`
}

type ResponseData struct {
	Token             string `json:"token"`
	RefreshToken      string `json:"refresh_token"`
	ExpiresIn         int64  `json:"expires_in"`
	Role              string `json:"role"`
	Email             string `json:"email"`
	Name              string `json:"name"`
	ShortName         string `json:"shortname"`
	UniqueOrderCode   string `json:"unique_order_code"`
	QiscusEmail       string `json:"qiscus_email"`
	ProfPicWithoutKey string `json:"profpic_without_key"`
	DeviceID          string `json:"device_id"`
	DeviceName        string `json:"device_name"`
	FirebaseToken     string `json:"firebase_token"`
}

type RefreshTokenReq struct {
	RefreshToken     string `form:"refresh_token" binding:"required" json:"refresh_token"`
	ClientID         string `form:"client_id" binding:"required" json:"client_id"`
	ClientSecret     string `form:"client_secret" binding:"required" json:"client_secret"`
	DeviceID         string `form:"device_id" binding:"omitempty" json:"device_id"`
	DeviceName       string `form:"device_name" binding:"omitempty" json:"device_name"`
	UseFirebaseToken string `form:"use_firebase_token" binding:"omitempty" json:"use_firebase_token"`
}

type RefreshTokenResponse struct {
	Anon              bool   `json:"anon"`
	Email             string `json:"email"`
	ExpiresIn         int64  `json:"expires_in"`
	FirebaseToken     string `json:"firebase_token"`
	Name              string `json:"name"`
	ProfpicWithoutKey string `json:"profpic_without_key"`
	QiscusEmail       string `json:"qiscus_email"`
	RefreshToken      string `json:"refresh_token"`
	Role              string `json:"role"`
	Shortname         string `json:"shortname"`
	Token             string `json:"token"`
	UniqueOrderCode   string `json:"unique_order_code"`
}

type GetUserParentResponseData struct {
	ShowParentalForm      bool   `json:"showParentalForm"`
	ParentName            string `json:"parentName"`
	ParentCellphoneNumber string `json:"parentCellphoneNumber"`
}

type TnCSubmissioneReq struct {
	ParentName            string `json:"parentName" binding:"required"`
	ParentCellphoneNumber string `json:"parentCellphoneNumber" binding:"required"`
	TncAgreed             string `json:"tncAgreed" binding:"required"`
}

type GetUserQuery struct {
	Name            string   `form:"name"`
	Email           string   `form:"email"`
	CellphoneNumber string   `form:"cellphone_number"`
	Emails          []string `form:"emails"`
	UserSerials     []string `form:"userSerials"`
	UserIDs         []string `form:"userIds"`
}

type UpdateUsersByAdminQuery struct {
	UniqueOrderCode string `json:"serial"`
	Name            string `json:"name"`
	Role            string `json:"role"`
}

type CreateUserRukerCMSBulkReq struct {
	Email          string `json:"email"`
	Name           string `json:"name"`
	Role           string `json:"role"`
	Phone          string `json:"phone"`
	HashedPassword string `json:"hashedPassword"`
}

type CreateUserRukerCMSBulkResp struct {
	UserSerial string `json:"userSerial"`
	Email      string `json:"email"`
	Status     string `json:"status"` // New | Existing | Failed
	ErrorMsg   string `json:"errorMsg"`
}

type UserParentStatus struct {
	Status bool `json:"status"`
}

type UserParentStatusBackward struct {
	Data StatusData `json:"data"`
}

type StatusData struct {
	StatusData bool `json:"status_data"`
}

type UserActivationStatus struct {
	Email    string `json:"email"`
	IsActive string `json:"is_active"`
}

type ChangePasswordReq struct {
	NewPasswordConfirmation string `form:"new_password_confirmation"`
	NewPassword             string `form:"new_password"`
	OldPassword             string `form:"old_password"`
}

type ForgotPasswordResp struct {
	KeyLostPassword string `json:"key_lost_password"`
}

type ForgotPasswordReq struct {
	Email                string `form:"email"`
	KeyLostPwd           string `form:"keyLostPwd"`
	Password             string `form:"password"`
	PasswordConfirmation string `form:"password_confirmation"`
}

type ChangeUserEmailReq struct {
	UserSerial string `json:"userSerial"`
	Email      string `json:"email"`
	Password   string `json:"password"`
}
