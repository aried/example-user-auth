package user

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type ErrUocOrReferralNotFound struct{}

func (e *ErrUocOrReferralNotFound) Error() string {
	return "UOC / Referral Code not found"
}

type ErrEmailNotFound struct{}

func (e *ErrEmailNotFound) Error() string {
	return "Email not found"
}

type ErrUserIDNotFound struct{}

func (e *ErrUserIDNotFound) Error() string {
	return "User ID not found"
}

type ErrUserHasBeenActivated struct{}

func (e *ErrUserHasBeenActivated) Error() string {
	return "user has been activated"
}

type Repository interface {
	GetUser(email string) (*Users, error)
	GetUserByUserID(userID int) (*Users, error)
	GetUserByUOC(uoc string) (*Users, error)
	GetUserAddressData(ctx context.Context, userID int) (*UserAddress, error)
	GetUserParentData(ctx context.Context, userID int) (*UserParent, error)
	GetUserExtraData(ctx context.Context, userID int) (*UserExtra, error)
	UpdateLastCheckParentalForm(ctx context.Context, userID int) error
	CreateUser(*Users, *UserExtra) error
	CreateUserParent(ctx context.Context, userParent UserParent) error
	CreateUserExtra(ctx context.Context, userParent UserExtra) error
	UpdateUserExtra(ctx context.Context, uExtra UserExtra) error
	UpdateUserPassword(ctx context.Context, userID int, newPassword string) error
	UpdateUserSubscribe(ctx context.Context, uExtra UserExtra) error
	UpdateTnCAgreed(ctx context.Context, userID int, parentName string, parentCellphoneNumber string) error
	GetUsersByQuery(ctx context.Context, query GetUserQuery) ([]Users, error)
	GetUsersByEmailOrCellphoneNumber(email string, cellphoneNumber string) (users Users, err error)
	UpdateUsersByAdmin(ctx context.Context, u *UpdateUsersByAdminQuery) error
	UpdateUsersByAdminInTransaction(ctx context.Context, users []UpdateUsersByAdminQuery) error
	UpdateSingleUser(ctx context.Context, user *Users) error
	GetUserByEmail(ctx context.Context, email string) (users Users, err error)
	GetUserByCellphoneNumber(ctx context.Context, cellphoneNumber string) (users Users, err error)
	GetUserByUserIDForUserProfile(ctx context.Context, userID string) (*Users, *UserAddress, *UserAdministrative, *UserExtra, *UserTeacher, *UserParent, error)
	GetUserByUocOrReferralCode(ctx context.Context, uoc string) (*Users, error)
	ActivateUserByEmail(ctx context.Context, user *Users) error
	UpdateUserAllTables(ctx context.Context, user *Users, userAddress *UserAddress, userAdministrative *UserAdministrative, userExtra *UserExtra, userTeacher *UserTeacher, userParent *UserParent, updateTableNeeded map[string]bool) (err error)
	GetAllUsersByQuery(ctx context.Context, query GetAllUsersQuery) ([]GetAllUsersByQueryResult, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) Repository {
	return repository{db}
}

func (r repository) GetAllUsersByQuery(ctx context.Context, query GetAllUsersQuery) ([]GetAllUsersByQueryResult, error) {
	db := r.db

	db = db.Table("users").Select("users.id, users.name, users.email, users.role, users.cellphone_number, users.is_active, users.unique_order_code, users.created_at, users.gender, users.profile_pic, users.grade_serial, user_address.domicile_city").Joins("left join user_address on user_address.user_id = users.id")

	if query.UserID != "" {
		db = db.Where("users.id = ?", query.UserID)
	}
	if query.Name != "" {
		db = db.Where("users.name = ?", query.Name)
	}
	if query.Email != "" {
		db = db.Where("users.email = ?", query.Email)
	}
	if query.Role != "" {
		db = db.Where("users.role = ?", query.Role)
	}
	if query.CellphoneNumber != "" {
		db = db.Where("users.cellphone_number = ?", query.CellphoneNumber)
	}
	if query.IsActive != "" {
		db = db.Where("users.is_active = ?", query.IsActive)
	}
	if query.UserSerial != "" {
		db = db.Where("users.id = ?", query.UserID)
	}
	if query.CreatedAt != "" {
		db = db.Where("users.created_at >= ? AND users.created_at <= ?", fmt.Sprintf("%s 00:00:00", query.CreatedAt), fmt.Sprintf("%s 23:59:59", query.CreatedAt))
	}
	if query.Gender != "" {
		db = db.Where("users.gender = ?", query.Gender)
	}
	if query.GradeSerial != "" {
		db = db.Where("users.grade_serial = ?", query.GradeSerial)
	}
	if query.Page != 0 && query.PageSize != 0 {
		offset := (query.Page - 1) * query.PageSize
		db = db.Limit(query.PageSize).Offset(offset).Order("id desc")
	}
	db = db.Where("users.deleted_at is null")

	var users []GetAllUsersByQueryResult
	err := db.Scan(&users).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}
	return users, nil
}

func (r repository) UpdateUserAllTables(ctx context.Context, user *Users, userAddress *UserAddress, userAdministrative *UserAdministrative, userExtra *UserExtra, userTeacher *UserTeacher, userParent *UserParent, updateTableNeeded map[string]bool) (err error) {
	tx := r.db.Begin()
	if updateTableNeeded["userTable"] {
		err = tx.Save(user).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if updateTableNeeded["userAddressTable"] {
		err = tx.Save(userAddress).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if updateTableNeeded["userExtraTable"] {
		err = tx.Save(userExtra).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if updateTableNeeded["userAdministrativeTable"] {
		err = tx.Save(userAdministrative).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if updateTableNeeded["userTeacherTable"] {
		err = tx.Save(userTeacher).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if updateTableNeeded["userParentTable"] {
		err = tx.Save(userParent).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}

func (r repository) ActivateUserByEmail(ctx context.Context, user *Users) error {
	err := r.db.Exec("UPDATE users SET is_active = 'Y', updated_at = NOW(), activated_at = NOW() WHERE email = ?", user.Email).Error
	if err != nil {
		return err
	}
	return nil
}

func (r repository) GetUserByUserIDForUserProfile(ctx context.Context, userID string) (*Users, *UserAddress, *UserAdministrative, *UserExtra, *UserTeacher, *UserParent, error) {
	user := &Users{}
	userAddress := &UserAddress{}
	userAdministrative := &UserAdministrative{}
	userExtra := &UserExtra{}
	userTeacher := &UserTeacher{}
	userParent := &UserParent{}

	tx := r.db.Begin()
	err := tx.First(user, "id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.First(userAddress, "user_id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.First(userAdministrative, "user_id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.First(userExtra, "user_id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.First(userTeacher, "user_id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.First(userParent, "user_id = ?", userID).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	err = tx.Commit().Error
	if err != nil && err != gorm.ErrRecordNotFound {
		tx.Rollback()
		return &Users{}, &UserAddress{}, &UserAdministrative{}, &UserExtra{}, &UserTeacher{}, &UserParent{}, err
	}
	return user, userAddress, userAdministrative, userExtra, userTeacher, userParent, nil
}

func (r repository) GetUserByEmail(ctx context.Context, email string) (users Users, err error) {
	err = r.db.Where("email = ?", strings.ToLower(email)).First(&users).Error
	if err == gorm.ErrRecordNotFound {
		return users, nil
	}
	return
}

func (r repository) GetUserByCellphoneNumber(ctx context.Context, cellphoneNumber string) (users Users, err error) {
	err = r.db.Where("cellphone_number = ?", cellphoneNumber).First(&users).Error
	if err == gorm.ErrRecordNotFound {
		return users, nil
	}
	return
}

func (r repository) GetUserByUocOrReferralCode(ctx context.Context, uoc string) (*Users, error) {
	ret := &Users{}
	err := r.db.First(ret, "unique_order_code = ? OR referral_code = ?", uoc, uoc).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return &Users{}, &ErrUocOrReferralNotFound{}
	}
	if err != nil {
		return &Users{}, err
	}
	return ret, nil
}

func (r repository) UpdateSingleUser(ctx context.Context, user *Users) error {
	return r.db.Save(user).Error
}

func (r repository) UpdateUsersByAdmin(ctx context.Context, u *UpdateUsersByAdminQuery) error {
	err := r.db.Exec("UPDATE users SET name = ?, role = ? WHERE unique_order_code = ?", u.Name, u.Role, u.UniqueOrderCode).Error
	if err != nil {
		return err
	}
	return nil
}

func (r repository) GetUsersByQuery(ctx context.Context, query GetUserQuery) ([]Users, error) {
	db := r.db

	if len(query.UserSerials) > 0 {
		db = db.Where("unique_order_code IN (?)", query.UserSerials)
	}
	if len(query.Emails) > 0 {
		db = db.Where("email IN (?)", query.Emails)
	}
	if len(query.UserIDs) > 0 {
		db = db.Where("id IN (?)", query.UserIDs)
	}
	if query.Email != "" && query.Name != "" {
		db = db.Where("email LIKE ? OR name LIKE ?", fmt.Sprintf("%%%s%%", query.Email), fmt.Sprintf("%%%s%%", query.Name))
	} else {
		if query.Email != "" {
			db = db.Where("email LIKE ?", fmt.Sprintf("%%%s%%", query.Email))
		} else if query.Name != "" {
			db = db.Where("name LIKE ?", fmt.Sprintf("%%%s%%", query.Name))
		}
	}
	var users []Users
	err := db.Find(&users).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return nil, err
	}

	return users, nil
}

func (r repository) GetUsersByEmailOrCellphoneNumber(email string, cellphoneNumber string) (users Users, err error) {
	err = r.db.Where("email = ? OR cellphone_number = ? ", email, cellphoneNumber).First(&users).Order("id DESC").Error
	if err == gorm.ErrRecordNotFound {
		return users, nil
	}
	return
}

func (r repository) UpdateTnCAgreed(ctx context.Context, userID int, parentName string, parentCellphoneNumber string) error {
	if err := r.db.Exec("UPDATE user_parent SET last_check_parental_form = NOW(), tnc_approved = 1, parent_name = ?, parent_cellphone_number = ? WHERE user_id= ?", parentName, parentCellphoneNumber, userID).Error; err != nil {
		return err
	}
	return nil
}

func (r repository) UpdateLastCheckParentalForm(ctx context.Context, userID int) error {
	if err := r.db.Exec("UPDATE user_parent SET last_check_parental_form = NOW(), tnc_approved = 0 WHERE user_id= ?", userID).Error; err != nil {
		return err
	}
	return nil
}

func (r repository) CreateUserParent(ctx context.Context, userParent UserParent) error {
	return r.db.Create(&userParent).Error
}

func (r repository) GetUserAddressData(ctx context.Context, userID int) (*UserAddress, error) {
	ret := &UserAddress{}
	err := r.db.First(ret, "user_id = ?", userID).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrUserIDNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) GetUserParentData(ctx context.Context, userID int) (*UserParent, error) {
	ret := &UserParent{}
	err := r.db.First(ret, "user_id = ?", userID).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrUserIDNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) CreateUserExtra(ctx context.Context, userExtra UserExtra) error {
	return r.db.Create(&userExtra).Error
}

func (r repository) UpdateUserExtra(ctx context.Context, userExtra UserExtra) error {
	return r.db.Table("user_extra").Where("user_id = (?)", userExtra.UserID).Updates(userExtra).Error
}

func (r repository) GetUserExtraData(ctx context.Context, userID int) (*UserExtra, error) {
	ret := &UserExtra{}
	err := r.db.First(ret, "user_id = ?", userID).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrUserIDNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) GetUserByUserID(userID int) (*Users, error) {
	ret := &Users{}
	err := r.db.First(ret, "id = ?", userID).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrUserIDNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) GetUserByUOC(uoc string) (*Users, error) {
	ret := &Users{}
	err := r.db.First(ret, "unique_order_code = ?", uoc).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrUserIDNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) GetUser(email string) (*Users, error) {
	ret := &Users{}
	err := r.db.First(ret, "email = ?", email).Error
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, &ErrEmailNotFound{}
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (r repository) UpdateUsersByAdminInTransaction(ctx context.Context, users []UpdateUsersByAdminQuery) error {
	var err error
	err = nil
	tx := r.db.Begin()
	for _, u := range users {
		err = r.db.Exec("UPDATE users SET name = ?, role = ? WHERE unique_order_code = ?", &u.Name, &u.Role, &u.UniqueOrderCode).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}

func (r repository) UpdateUserPassword(ctx context.Context, userID int, newPassword string) error {
	if err := r.db.Exec("UPDATE users SET password = ?, updated_at = ? WHERE id= ?", newPassword, time.Now(), userID).Error; err != nil {
		return err
	}
	return nil
}

func (r repository) UpdateUserSubscribe(ctx context.Context, uExtra UserExtra) error {
	if err := r.db.Exec("UPDATE user_extra SET subscribe = ?, updated_at = ? WHERE user_id= ?", uExtra.Subscribe, time.Now(), uExtra.UserID).Error; err != nil {
		return err
	}
	return nil
}

func (r repository) CreateUser(u *Users, uExtra *UserExtra) (err error) {
	tx := r.db.Begin()
	err = tx.Create(u).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	userAddress := &UserAddress{}
	userAddress.UserID = u.ID
	err = tx.Create(userAddress).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	uExtra.UserID = u.ID
	err = tx.Create(uExtra).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	userAdministrative := &UserAdministrative{}
	userAdministrative.UserID = u.ID
	err = tx.Create(userAdministrative).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	userTeacher := &UserTeacher{}
	userTeacher.UserID = u.ID
	err = tx.Create(userTeacher).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	userParent := &UserParent{}
	userParent.UserID = u.ID
	err = tx.Create(userParent).Error
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}
