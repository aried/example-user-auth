package email

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/ruangguru/source/shared-lib/go/fault"
)

const (
	timeout = 10
)

type service struct {
	baseURL string
}

// Service define available remote call procedure to user auth service
type Service interface {
	SendActivationEmail(req ActivationEmailReq, isStudent bool) error
	SendForgotPassword(name string, email string, key string) error
	SendResetPassword(name string, email string, password string) error
}

// NewService - default constructor
func NewService(baseURL string) Service {
	return &service{baseURL}
}

func (s *service) SendActivationEmail(req ActivationEmailReq, isStudent bool) error {
	baseURL := fmt.Sprintf("%s/api/v3/teacher-confirm?", s.baseURL)
	if isStudent {
		baseURL = fmt.Sprintf("%s/api/v3/student-confirm?", s.baseURL)
	}
	paramURL := fmt.Sprintf("&email=%s&name=%s&activation_code=%s",
		url.QueryEscape(req.UserEmail),
		url.QueryEscape(req.UserName),
		url.QueryEscape(req.ActivationCode))
	targetURL := baseURL + paramURL
	client := &http.Client{}

	request, _ := http.NewRequest("GET", targetURL, nil)
	request.Header.Add("Accept", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, err.Error())
	}

	if resp.StatusCode != 200 {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, "Send activation email failed!")
	}
	defer resp.Body.Close()
	return nil
}

func (s *service) SendForgotPassword(name string, email string, key string) error {
	baseURL := fmt.Sprintf("%s/api/v3/forget-password?", s.baseURL)
	paramURL := fmt.Sprintf("&name=%s&e=%s&q=%s",
		url.QueryEscape(name),
		url.QueryEscape(email),
		url.QueryEscape(key))
	targetURL := baseURL + paramURL
	client := &http.Client{
		Timeout: timeout * time.Second,
	}

	request, _ := http.NewRequest("GET", targetURL, nil)
	request.Header.Add("Accept", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, err.Error())
	}

	if resp.StatusCode != 200 {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, "Forgot password failed!")
	}

	defer resp.Body.Close()
	return nil
}

func (s *service) SendResetPassword(name string, email string, password string) error {
	targetURL := fmt.Sprintf("%s/api/v3/reset-password", s.baseURL)
	requestData := ResetPasswordReq{
		Email:    email,
		Name:     name,
		Password: password,
	}
	requestJSON, err := json.Marshal(requestData)
	if err != nil {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, err.Error())
	}
	req, err := http.NewRequest("POST", targetURL, bytes.NewBuffer(requestJSON))
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)

	if err != nil {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, err.Error())
	}

	if resp.StatusCode != 200 {
		return fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, "Reset password failed!")
	}
	defer resp.Body.Close()
	return nil
}
