package email

type ActivationEmailReq struct {
	UserName       string
	UserEmail      string
	ActivationCode string
}

type ResetPasswordReq struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}
