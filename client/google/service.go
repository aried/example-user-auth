package google

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/ruangguru/source/user-auth/user"
)

type Service interface {
	GetProfileGoogle(r user.LoginReq) (GoogleResp, error)
}

type service struct{}

func NewService() Service {
	return &service{}
}

func (s *service) GetProfileGoogle(r user.LoginReq) (GoogleResp, error) {
	targetURL := strings.Join([]string{"https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=", r.Username}, "")

	resp, err := http.Get(targetURL)
	if err != nil {
		return GoogleResp{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return GoogleResp{}, err
	}
	var result GoogleResp
	err = json.Unmarshal(body, &result)
	if err != nil {
		return GoogleResp{}, err
	}
	return result, err
}
