package grade

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/ruangguru/source/user-auth/config"
)

const (
	timeout = 10
)

type service struct {
}

type Service interface {
	GetGrade(lessonName string) (GradeResponse, error)
	GetStudentClass() (StudentClassResponse, error)
}

func NewService() Service {
	return &service{}
}

func (s *service) GetGrade(lessonName string) (GradeResponse, error) {
	targetURL := fmt.Sprintf("%v/grade?search=%v", config.Get().KongInternalUrl, lessonName)

	req, err := http.NewRequest("GET", targetURL, nil)

	client := &http.Client{
		Timeout: timeout * time.Second,
	}

	resp, err := client.Do(req)
	if err != nil {
		return GradeResponse{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return GradeResponse{}, err
	}

	var result GradeResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return GradeResponse{}, err
	}

	return result, err
}

func (s *service) GetStudentClass() (StudentClassResponse, error) {
	req, err := http.NewRequest("GET", config.Get().StudentClassEndpoint, nil)
	client := &http.Client{
		Timeout: timeout * time.Second,
	}

	resp, err := client.Do(req)
	if err != nil {
		return StudentClassResponse{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return StudentClassResponse{}, err
	}

	var result StudentClassResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return StudentClassResponse{}, err
	}

	return result, err
}
