package grade

type GradeResponse struct {
	Status string `json:"status"`
	Data   []struct {
		Serial      string `json:"serial"`
		DisplayName string `json:"display_name"`
		Description string `json:"description"`
		GradeLevel  struct {
			Serial      string `json:"serial"`
			DisplayName string `json:"display_name"`
			Description string `json:"description"`
		} `json:"grade_level"`
		Extras    interface{} `json:"extras"`
		Kurikulum []string    `json:"kurikulum"`
		Created   struct {
			By   string `json:"by"`
			Date string `json:"date"`
		} `json:"created"`
		Updated struct {
			By   string `json:"by"`
			Date string `json:"date"`
		} `json:"updated"`
		Rank      int `json:"rank"`
		LevelRank int `json:"level_rank"`
	} `json:"data"`
	Message string `json:"message"`
}

type StudentClassResponse struct {
	Status string `json:"status"`
	Data   []struct {
		Name         string `json:"name"`
		Serial       string `json:"serial"`
		OptionsTitle string `json:"options_title"`
		Options      []struct {
			Name   string `json:"name"`
			Serial string `json:"serial"`
		} `json:"options"`
	} `json:"data"`
}
