package credit

type CreditResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
