package credit

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ruangguru/source/user-auth/config"
)

const (
	timeout = 10
)

type service struct {
}

type Service interface {
	AddKoin(UserId int, amount int) error
}

func NewService() Service {
	return &service{}
}

func (s *service) AddKoin(userId int, amount int) error {
	targetURL := fmt.Sprintf("%v/api/v2/credit/add-koin-by-service", config.Get().RgCoreHost)

	userIdString := strconv.Itoa(userId)
	amountString := strconv.Itoa(amount)

	form := url.Values{}
	form.Add("user_id", userIdString)
	form.Add("amount", amountString)

	req, err := http.NewRequest("POST", targetURL, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("secret-key", config.Get().RgCoreSecretKey)

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New("Add Koin Client Response not 200")
	}

	return nil
}
