package auth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/ruangguru/source/shared-lib/go/fault"

	"gitlab.com/ruangguru/source/shared-lib/go/client/auth"
)

const (
	timeout = 10
)

type service struct {
	baseURL   string
	secretKey string
}

// Service define available remote call procedure to user auth service
type Service interface {
	auth.Service
	AuthUser(username string, password string, role string, loginType string, clientID string, clientSecret string) (UserAuthResponse, error)
	AuthTokenUser(role string, clientID string, clientSecret string) (UserTokenResponse, error)
}

// NewService - default constructor
func NewService(baseURL string, secretKey string) Service {
	return &service{baseURL, secretKey}
}

// Remote call to auth service for token validation. Implement shared-lib interface
func (s *service) ValidateToken(token string) (auth.ValidateTokenResponse, error) {
	targetURL := s.baseURL + "/api/v3/auth/validate"

	form := url.Values{}
	form.Add("token", token)

	req, err := http.NewRequest("POST", targetURL, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		errMsg := fmt.Sprintf("Validate Token Client Call error : %v ", err.Error())
		return auth.ValidateTokenResponse{}, fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, errMsg)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errMsg := fmt.Sprintf("Validate Token Body Read error : %v ", err.Error())
		return auth.ValidateTokenResponse{}, fault.ErrorDictionary(fault.GeneralInternalClientAPIBodyReadError, errMsg)
	}

	var result auth.ValidateTokenResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		errMsg := fmt.Sprintf("Validate Token JSON Response error : %v. Body : %v ", err.Error(), string(body))
		return auth.ValidateTokenResponse{}, fault.ErrorDictionary(fault.GeneralJSONErrorCode, errMsg)
	}

	if result.Code != http.StatusOK {
		err := fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, fmt.Sprintf("Validate Token client error %v : %v", result.Code, result.Message))
		return auth.ValidateTokenResponse{}, err
	}

	return result, err
}

// Remote call to auth service for login authentication
func (s *service) AuthUser(username string, password string, role string, loginType string, clientID string, clientSecret string) (UserAuthResponse, error) {
	targetURL := s.baseURL + "/api/v3/auth"

	form := url.Values{}
	form.Add("username", username)
	form.Add("password", password)
	form.Add("role", role)
	form.Add("type", loginType)
	form.Add("client_id", clientID)
	form.Add("client_secret", clientSecret)

	req, err := http.NewRequest("POST", targetURL, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User Client Call error : %v ", err.Error())
		return UserAuthResponse{}, fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, errMsg)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User Body Read error : %v ", err.Error())
		return UserAuthResponse{}, fault.ErrorDictionary(fault.GeneralInternalClientAPIBodyReadError, errMsg)
	}

	var result UserAuthResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User JSON Response error : %v. Body : %v ", err.Error(), string(body))
		return UserAuthResponse{}, fault.ErrorDictionary(fault.GeneralJSONErrorCode, errMsg)
	}

	if result.Code != http.StatusOK {
		err := fault.ErrorDictionary(fault.GeneralInternalClientAPIRequestError, fmt.Sprintf("Auth User client error %v : %v", result.Code, result.Message))
		return UserAuthResponse{}, err
	}

	return result, err
}

func (s *service) AuthTokenUser(role string, clientID string, clientSecret string) (UserTokenResponse, error) {
	targetURL := s.baseURL + "/api/v3/auth/token"

	payload := UserTokenReq{
		Role:         role,
		ClientID:     clientID,
		ClientSecret: clientSecret,
	}

	payloadJSON, err := json.Marshal(payload)
	if err != nil {
		return UserTokenResponse{}, err
	}

	req, err := http.NewRequest("POST", targetURL, bytes.NewBuffer(payloadJSON))
	req.Header.Add("Content-Type", "application/json")

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User Client Call error : %v ", err.Error())
		return UserTokenResponse{}, fault.ErrorDictionary(fault.GeneralRequestTokenUnavailableError, errMsg)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User Body Read error : %v ", err.Error())
		return UserTokenResponse{}, fault.ErrorDictionary(fault.GeneralInternalClientAPIBodyReadError, errMsg)
	}

	var result UserTokenResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		errMsg := fmt.Sprintf("Auth User JSON Response error : %v. Body : %v ", err.Error(), string(body))
		return UserTokenResponse{}, fault.ErrorDictionary(fault.GeneralJSONErrorCode, errMsg)
	}

	if result.Code != http.StatusOK {
		err := fault.ErrorDictionary(fault.GeneralRequestTokenUnavailableError, fmt.Sprintf("Auth User client error %v : %v", result.Code, result.Message))
		return UserTokenResponse{}, err
	}

	return result, err
}
