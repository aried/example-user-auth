package auth

import (
	"gitlab.com/ruangguru/source/shared-lib/go/client/auth"
)

// UserAuthResponse used to marshall response from POST /auth
type UserAuthResponse struct {
	Code int `json:"code"`
	Data []struct {
		RefreshToken string `json:"refresh_token"`
		Role         string `json:"role"`
		Token        string `json:"token"`
		UserSerial   string `json:"unique_order_code"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

// RefreshTokenResponse used to marshall response from POST /auth/refresh-token
type RefreshTokenResponse struct {
	Code int `json:"code"`
	Data []struct {
		DevideID     string `json:"device_id"`
		DeviceName   string `json:"device_name"`
		RefreshToken string `json:"refresh_token"`
		Role         string `json:"role"`
		Token        string `json:"token"`
		UserCode     string `json:"unique_order_code"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

// ForgotPasswordResponse used to marshall response from POST /user/forgot-password
type ForgotPasswordResponse struct {
	Code int `json:"code"`
	Data []struct {
		KeyLostPassword string `json:"key_lost_password"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

// CreateUserbaseResponse used to marshall response from new registered user via POST /user
type CreateUserbaseResponse struct {
	Code int `json:"code"`
	Data []struct {
		ActivationCode int    `json:"activation_code"`
		UserCode       string `json:"user_code"`
		UserID         int    `json:"user_id"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

// UserInfoResponse used to marshall response from GET /user/find
type UserInfoResponse struct {
	Code int `json:"code"`
	Data []struct {
		ActivationCode       string `json:"activation_code"`
		BirthDate            string `json:"birth_date"`
		BirthPlace           string `json:"birth_place"`
		Phone                string `json:"cellphone_number"`
		CreatedAt            string `json:"created_at"`
		Email                string `json:"email"`
		Gender               string `json:"gender"`
		IsActive             string `json:"is_active"`
		Name                 string `json:"name"`
		ProfilePic           string `json:"profile_pic"`
		ProfilePicFullDomain string `json:"profile_pic_full_domain"`
		ProfilePicNoKey      string `json:"profpic_without_key"`
		Role                 string `json:"role"`
		Shortname            string `json:"shortname"`
		UserCode             string `json:"unique_order_code"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

// UserInfoByEmailResponse used to marshall response from GET /user/search?email=
type UserInfoByEmailResponse struct {
	Code int `json:"code"`
	Data []struct {
		ActivationCode       string `json:"activation_code"`
		BirthDate            string `json:"birth_date"`
		BirthPlace           string `json:"birth_place"`
		Phone                string `json:"cellphone_number"`
		CreatedAt            string `json:"created_at"`
		Email                string `json:"email"`
		Gender               string `json:"gender"`
		IsActive             string `json:"is_active"`
		Name                 string `json:"name"`
		ProfilePic           string `json:"profile_pic"`
		ProfilePicFullDomain string `json:"profile_pic_full_domain"`
		ProfilePicNoKey      string `json:"profpic_without_key"`
		Role                 string `json:"role"`
		Shortname            string `json:"shortname"`
		UserCode             string `json:"unique_order_code"`
	} `json:"data"`
	Status    string                  `json:"status"`
	Error     string                  `json:"error"`
	Message   string                  `json:"message"`
	ErrorDesc []auth.ErrorDescription `json:"error_description"`
}

type UserTokenResponse struct {
	Status string `json:"status"`
	Data   []struct {
		Token        string `json:"token"`
		RefreshToken string `json:"refreshToken"`
		Role         string `json:"role"`
	} `json:"data"`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type UserTokenReq struct {
	ClientID     string `json:"clientID"`
	ClientSecret string `json:"clientSecret"`
	Role         string `json:"role"`
}
