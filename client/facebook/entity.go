package facebook

type FacebookResp struct {
	ID     string `json:"id"`
	Gender string `json:"gender"`
	Email  string `json:"email"`
	Name   string `json:"name"`
}
