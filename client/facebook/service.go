package facebook

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/ruangguru/source/user-auth/user"
)

type Service interface {
	GetProfileFacebook(r user.LoginReq) (FacebookResp, error)
}

type service struct{}

func NewService() Service {
	return &service{}
}

func (s *service) GetProfileFacebook(r user.LoginReq) (FacebookResp, error) {
	targetURL := strings.Join([]string{"https://graph.facebook.com/v2.12/me?fields=id,gender,email,name&access_token=", r.Username}, "")

	resp, err := http.Get(targetURL)
	if err != nil {
		return FacebookResp{}, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return FacebookResp{}, err
	}
	var result FacebookResp
	err = json.Unmarshal(body, &result)
	if err != nil {
		return FacebookResp{}, err
	}
	return result, err
}
