package region

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/ruangguru/source/user-auth/config"
)

const (
	timeout = 10
)

type service struct {
}

type Service interface {
	GetProvince(serial []int) (ProvinceResponse, error)
	GetCity(serial []int) (CityResponse, error)
}

func NewService() Service {
	return &service{}
}

func (s *service) GetCity(serials []int) (CityResponse, error) {
	var serialsString string
	var serialsStringSlice []string
	for _, serial := range serials {
		serialsStringSlice = append(serialsStringSlice, strconv.Itoa(serial))
	}
	serialsString = strings.Join(serialsStringSlice, ",")

	targetURL := fmt.Sprintf("%v/city?result_type=lazy-load&serials=[%v]", config.Get().KongInternalUrl, serialsString)

	req, err := http.NewRequest("GET", targetURL, nil)

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return CityResponse{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return CityResponse{}, err
	}

	var result CityResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return CityResponse{}, err
	}

	return result, err
}

func (s *service) GetProvince(serial []int) (ProvinceResponse, error) {
	var serialString string
	if len(serial) == 1 {
		serialString = strconv.Itoa(serial[0])
	} else if len(serial) == 2 {
		serialString = fmt.Sprintf("%v,%v", serial[0], serial[1])
	}
	targetURL := fmt.Sprintf("%v/province?result_type=lazy-load&serials=[%v]", config.Get().KongInternalUrl, serialString)

	req, err := http.NewRequest("GET", targetURL, nil)

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return ProvinceResponse{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ProvinceResponse{}, err
	}

	var result ProvinceResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return ProvinceResponse{}, err
	}

	return result, err
}
