package region

type provinceMap map[string]Province

type ProvinceResponse struct {
	Status  string      `json:"status"`
	Data    provinceMap `json:"data"`
	Message string      `json:"message"`
}

type Province struct {
	Serial      int         `json:"serial"`
	DisplayName string      `json:"display_name"`
	Extras      interface{} `json:"extras"`
	Created     struct {
		By   string `json:"by"`
		Date string `json:"date"`
	} `json:"created"`
	Updated struct {
		By   string `json:"by"`
		Date string `json:"date"`
	} `json:"updated"`
}

type cityMap map[string]City

type CityResponse struct {
	Status  string  `json:"status"`
	Data    cityMap `json:"data"`
	Message string  `json:"message"`
}

type City struct {
	Serial      int    `json:"serial"`
	DisplayName string `json:"display_name"`
	Province    struct {
		Serial      int    `json:"serial"`
		DisplayName string `json:"display_name"`
	} `json:"province"`
	Extras  interface{} `json:"extras"`
	Created struct {
		By   string `json:"by"`
		Date string `json:"date"`
	} `json:"created"`
	Updated struct {
		By   string `json:"by"`
		Date string `json:"date"`
	} `json:"updated"`
}
