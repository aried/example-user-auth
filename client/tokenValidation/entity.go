package tokenValidation

type ValidateResponse struct {
	Code int `json:"code"`
	Data []struct {
		RefreshToken string `json:"refresh_token"`
		Role         string `json:"role"`
		Token        string `json:"token"`
		UserCode     string `json:"user_code"`
		UserID       int    `json:"user_id"`
	} `json:"data"`
	Status string `json:"status"`
}
