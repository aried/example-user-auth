package tokenValidation

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/ruangguru/source/user-auth/config"
)

const (
	timeout = 10
)

type service struct {
}

type Service interface {
	ValidateToken(token string) (ValidateResponse, error)
}

func NewService() Service {
	return &service{}
}

func (s *service) ValidateToken(token string) (ValidateResponse, error) {

	targetURL := config.Get().ValidateTokenEndpoint

	form := url.Values{}
	form.Add("token", token)

	req, err := http.NewRequest("POST", targetURL, strings.NewReader(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{
		Timeout: timeout * time.Second,
	}
	resp, err := client.Do(req)
	if err != nil {
		return ValidateResponse{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return ValidateResponse{}, err
	}

	var result ValidateResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return ValidateResponse{}, err
	}

	return result, err
}
