package serialutil

import (
	"strings"

	"gitlab.com/ruangguru/source/shared-lib/go/utils/serial"
)

func GenerateUniqueOrderCodeFromName(name string) string {
	newSerial := serial.New(getSerialPrefixFromName(name))
	return newSerial.String()
}

func getSerialPrefixFromName(name string) string {
	// make lowercase + clean word
	lowerName := strings.ToLower(name)
	nameSplit := strings.Split(lowerName, " ")
	firstName := strings.TrimSpace(strings.Trim(nameSplit[0], "."))

	// if name is empty, return default string
	if len(firstName) <= 1 || firstName == "" {
		return "USER"
	}

	// if name is in 'pasaran' list, check for the 2nd name and beyond
	switch firstName {
	case
		"muhammad", "muhamad", "nur", "siti", "desti", "ahmad", "m", "putri", "nurul", "sri",
		"muh", "i", "dwi", "ni", "ayu", "annisa", "dewi", "andi", "wahyu", "dimas", "abdul",
		"rizky", "indah", "rizki", "intan", "eka", "nabila", "ade", "fitri", "tri", "dian",
		"agus", "aulia", "ilham", "anisa", "agung", "dinda", "reza", "moh", "tiara", "riska",
		"mohammad", "nadia", "aditya", "m.", "fajar", "devi":
		if len(nameSplit) > 1 {
			// Nama pasaran, use combination with 2nd word
			firstName = nameSplit[1] + firstName[0:1]
		}
	}

	// if shorter than expected, add randomized character
	if len(firstName) < 8 {
		// name is less than 8 char, use longer randomized character
		addRandomChar := serial.NewRandomLength("", 8-len(firstName))
		return firstName + addRandomChar.String()
	} else {
		// if longer than expected, use the 1st 8 character only
		return firstName[0:8]
	}
}
