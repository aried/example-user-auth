package validationutil

import "regexp"

func ValidateCellphoneNumber(cellphoneNumber string) bool {
	if ok, _ := regexp.MatchString(`^62[0-9]{8,15}$`, cellphoneNumber); !ok {
		return false
	}
	return true
}

func ValidateGender(gender string) bool {
	if gender == "M" || gender == "F" {
		return true
	}
	return false
}

func ValidateRegistrationRole(role string) bool {
	if role == "student" || role == "teacher" || role == "employee" {
		return true
	}
	return false
}
