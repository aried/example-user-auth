package api

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ruangguru/source/shared-lib/go/api"
	"gitlab.com/ruangguru/source/shared-lib/go/fault"
	"gitlab.com/ruangguru/source/shared-lib/go/utils/requests"
	"gitlab.com/ruangguru/source/user-auth/client/auth"
	"gitlab.com/ruangguru/source/user-auth/client/region"
	"gitlab.com/ruangguru/source/user-auth/config"
	"gitlab.com/ruangguru/source/user-auth/user"
)

type UserHTTPHandler interface {
	CheckAvailableEmailAndCellphoneNumber(c *gin.Context)
	UpdateUserSubcribe(c *gin.Context)
	GetUserPercentage(c *gin.Context)
	CheckUserParent(c *gin.Context)
	CheckUserActive(c *gin.Context)
	ResendActivation(c *gin.Context)
	CreateUserParent(c *gin.Context)
	Register(c *gin.Context)
	GetUserDataFromToken(c *gin.Context)
	GetUserDataFromUocOrReferralCode(c *gin.Context)
	GetUserDataByUserId(c *gin.Context)
	SearchUserByEmail(c *gin.Context)
	SearchUserByEmails(c *gin.Context)
	SearchUserByUocs(c *gin.Context)
	SearchUserByUserIDs(c *gin.Context)
	ChangePassword(c *gin.Context)
	GetForgotPassword(c *gin.Context)
	SetForgotPassword(c *gin.Context)
	ActivateUserByEmail(c *gin.Context)
	UpdateUser(c *gin.Context)
}

type userHTTPHandler struct {
	UserSvc   user.Service
	AuthSvc   auth.Service
	RegionSvc region.Service
	config    config.Config
}

func NewUserHTTPHandler(userSvc user.Service, authSvc auth.Service, regionSvc region.Service, config config.Config) UserHTTPHandler {
	handler := userHTTPHandler{userSvc, authSvc, regionSvc, config}
	return &handler
}

const (
	EmailHadBeenRegisteredErrorCode        int = 50100
	MobileNumberHadBeenRegisteredErrorCode int = 50101
	PasswordNotEnoughCharacterErrorCode    int = 50102
	LessonNameValueNotAllowedErrorCode     int = 60109
)

func (h *userHTTPHandler) UpdateUser(c *gin.Context) {
	var payload user.UpdateFormRequest
	err := c.ShouldBind(&payload)
	if err != nil {
		err = ValidationError(err)
		ResponseFailed(c, err)
		return
	}

	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	userId := c.GetHeader("UserID")
	respData, err := h.UserSvc.UpdateUser(ctx, payload, userId)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	ResponseSuccess(c, http.StatusOK, respData, "success")
	return
}

func (h *userHTTPHandler) ActivateUserByEmail(c *gin.Context) {
	email := c.Query("email")
	activationCode := c.Query("activation_code")
	if email == "" {
		err := errorResponse("email", "required field")
		ResponseFailed(c, err)
		return
	}
	if activationCode == "" {
		err := errorResponse("activation code", "required field")
		ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	err := h.UserSvc.ActivateUserByEmail(ctx, email, activationCode)
	if err != nil {
		switch err.(type) {
		case *user.ErrEmailNotFound:
			errEmailNotFoundResponse := errorResponse("email", "not found")
			ResponseFailed(c, errEmailNotFoundResponse)
			return
		case *user.ActivationCodeNotMatchError:
			errActivationCodeInvalidResponse := errorResponse("activation code", "activation code invalid")
			ResponseFailed(c, errActivationCodeInvalidResponse)
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}
	c.Redirect(http.StatusMovedPermanently, "https://ruangguru.com/login/student")
	return
}

func (h *userHTTPHandler) SearchUserByUocs(c *gin.Context) {
	internalAPIRequestPassword := c.GetHeader("InternalAPIPassword")
	secretKey := c.GetHeader("SECRET-KEY")
	if secretKey == "" {
		secretKey = c.GetHeader("SECRET_KEY")
	}
	if secretKey == "" && internalAPIRequestPassword == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key/password", Message: "Either Secret key or password is required"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	if secretKey != "" && secretKey != h.config.SecretKeyGetProfile {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key", Message: "Secret key not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	if internalAPIRequestPassword != "" && internalAPIRequestPassword != h.config.InternalAPIRequestPassword {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "internal API password", Message: "internal API password not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	uocs := c.Query("uocs")
	if uocs == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "UOC", Message: "list of UOCs needed"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "list of UOCs needed",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	var query user.GetUserQuery
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	query.UserSerials = requests.QueryArray(c, "uocs")
	users, err := h.UserSvc.GetUsersByQuery(ctx, query)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	resp := Response{
		Status:  "success",
		Data:    users,
		Message: "user profile data",
		Code:    http.StatusOK,
	}
	c.JSON(http.StatusOK, resp)
	return
}

func (h *userHTTPHandler) SearchUserByEmails(c *gin.Context) {
	secretKey := c.GetHeader("SECRET-KEY")
	if secretKey == "" {
		secretKey = c.GetHeader("SECRET_KEY")
	}
	if secretKey != h.config.SecretKeyGetProfile {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key", Message: "Secret key not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	emails := c.Query("emails")
	if emails == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "list of emails needed"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "list of emails needed",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	var query user.GetUserQuery
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	query.Emails = requests.QueryArray(c, "emails")
	users, err := h.UserSvc.GetUsersByEmails(ctx, query)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	resp := Response{
		Status:  "success",
		Data:    users,
		Message: "user profile data",
		Code:    http.StatusOK,
	}
	c.JSON(http.StatusOK, resp)
	return
}

func (h *userHTTPHandler) SearchUserByUserIDs(c *gin.Context) {
	secretKey := c.GetHeader("SECRET-KEY")
	if secretKey == "" {
		secretKey = c.GetHeader("SECRET_KEY")
	}
	if secretKey != h.config.SecretKeyGetProfile {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key", Message: "Secret key not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	userIDs := c.Query("userIds")
	if userIDs == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "userIds", Message: "list of userId needed"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "list of userIds needed",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	var query user.GetUserQuery
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	query.UserIDs = requests.QueryArray(c, "userIds")
	users, err := h.UserSvc.GetUsersByUserIDs(ctx, query)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	resp := Response{
		Status:  "success",
		Data:    users,
		Message: "user profile data",
		Code:    http.StatusOK,
	}
	c.JSON(http.StatusOK, resp)
	return
}

func (h *userHTTPHandler) SearchUserByEmail(c *gin.Context) {
	secretKey := c.GetHeader("SECRET-KEY")
	if secretKey == "" {
		secretKey = c.GetHeader("SECRET_KEY")
	}
	if secretKey != h.config.SecretKeyGetProfile {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key", Message: "Secret key not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	email := c.Query("email")
	if email == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "required field"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "validation_fails",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	responseData, err := h.UserSvc.SearchUserByEmail(ctx, email)
	if err != nil {
		switch err.(type) {
		case *user.ErrEmailNotFound:
			var errDetails []ErrorDetail
			errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "not found"})
			UserIdNotFoundResponse := ErrorResponse{
				Status:           "error",
				ErrorType:        "validation_fails",
				Code:             404,
				ErrorDescription: errDetails,
			}
			ResponseFailed(c, UserIdNotFoundResponse)
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}
	ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func (h *userHTTPHandler) GetUserDataByUserId(c *gin.Context) {
	secretKey := c.GetHeader("SECRET-KEY")
	if secretKey == "" {
		secretKey = c.GetHeader("SECRET_KEY")
	}
	if secretKey != h.config.SecretKeyGetProfile {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "secret key", Message: "Secret key not valid"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "unauthorized",
			Code:             401,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	userId := c.Param("userId")
	if userId == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "user ID", Message: "required field"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "validation_fails",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	responseData, err := h.UserSvc.GetUserByUserId(ctx, userId)
	if err != nil {
		switch err.(type) {
		case *user.ErrUserIDNotFound:
			var errDetails []ErrorDetail
			errDetails = append(errDetails, ErrorDetail{Field: "user ID", Message: "not found"})
			UserIdNotFoundResponse := ErrorResponse{
				Status:           "error",
				ErrorType:        "validation_fails",
				Code:             404,
				ErrorDescription: errDetails,
			}
			ResponseFailed(c, UserIdNotFoundResponse)
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}
	ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func (h *userHTTPHandler) GetUserDataFromUocOrReferralCode(c *gin.Context) {
	uoc := c.Query("uoc")
	if uoc == "" {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "uoc", Message: "required field"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "validation_fails",
			Code:             400,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	responseData, err := h.UserSvc.GetUserByUocOrReferralCode(ctx, uoc)
	if err != nil {
		switch err.(type) {
		case *user.ErrUocOrReferralNotFound:
			var errDetails []ErrorDetail
			errDetails = append(errDetails, ErrorDetail{Field: "uoc", Message: "not found"})
			UocOrReferralNotFoundResponse := ErrorResponse{
				Status:           "error",
				ErrorType:        "validation_fails",
				Code:             404,
				ErrorDescription: errDetails,
			}
			ResponseFailed(c, UocOrReferralNotFoundResponse)
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}
	ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func (h *userHTTPHandler) GetUserDataFromToken(c *gin.Context) {
	anon := c.GetHeader("Anon")
	if strings.ToLower(anon) == "true" {
		err := fault.ErrorDictionary(fault.HTTPUnauthorizedError, "Guest Token used")
		api.ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	userID := c.GetHeader("UserID")
	responseData, err := h.UserSvc.GetUserByUserIDForUserProfile(ctx, userID)
	uID, err := strconv.Atoi(userID)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	userParentData, err := h.UserSvc.GetUserParentData(ctx, uID)
	if err != nil {
		switch err.(type) {
		case *user.ErrUserIDNotFound:
			var up user.UserParent
			up.UserID = uID
			localTime := getCurrentJakartaTime()
			up.CreatedAt = localTime
			approved := false
			up.TncApproved = &approved
			up.LastCheckParentalForm = &localTime
			h.UserSvc.CreateUserParent(ctx, up)
			responseData.ShowParentalForm = true
			api.ResponseSuccess(c, http.StatusOK, responseData, "Success")
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}

	if userParentData.TncApproved == nil || *userParentData.TncApproved == false {
		if userParentData.LastCheckParentalForm == nil {
			err := h.UserSvc.UpdateLastCheckParentalForm(ctx, uID)
			if err != nil {
				api.ResponseFailed(c, err)
				return
			}
			responseData.ShowParentalForm = true
			api.ResponseSuccess(c, http.StatusOK, responseData, "Success")
			return
		}
		timeToCompare := (userParentData.LastCheckParentalForm).Add(time.Minute * time.Duration(h.config.DurationBeforeShowingParentalForm))
		if timeToCompare.Before(getCurrentJakartaTime()) {
			err := h.UserSvc.UpdateLastCheckParentalForm(ctx, uID)
			if err != nil {
				api.ResponseFailed(c, err)
				return
			}
			responseData.ShowParentalForm = true
			api.ResponseSuccess(c, http.StatusOK, responseData, "Success")
			return
		}
	}

	responseData.ShowParentalForm = false
	ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func (h *userHTTPHandler) Register(c *gin.Context) {
	var req user.RegisterReq
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	req.Token = c.GetHeader("Authorization")
	if err := c.Bind(&req); err != nil {
		err = ValidationError(err)
		ResponseFailed(c, err)
		return
	}
	if len(req.Password) < 6 {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "password", Message: "Kata Sandi minimal 6 karakter"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "validation_fails",
			Code:             400,
			ErrorCode:        PasswordNotEnoughCharacterErrorCode,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	userFromEmail, err := h.UserSvc.GetUserByEmail(ctx, req.Email)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	if userFromEmail.Email == req.Email {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "Email sudah terdaftar"})
		err := ErrorResponse{
			Status:           "error",
			ErrorType:        "validation_fails",
			Code:             400,
			ErrorCode:        EmailHadBeenRegisteredErrorCode,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	if req.CellphoneNumber != "" {
		userFromMobile, err := h.UserSvc.GetUserByCellphoneNumber(ctx, req.CellphoneNumber)
		if err != nil {
			ResponseFailed(c, err)
			return
		}
		if userFromMobile.CellphoneNumber == req.CellphoneNumber {
			var errDetails []ErrorDetail
			errDetails = append(errDetails, ErrorDetail{Field: "cellphone_number", Message: "Nomor Handphone sudah terdaftar"})
			err := ErrorResponse{
				Status:           "error",
				ErrorType:        "validation_fails",
				Code:             400,
				ErrorCode:        MobileNumberHadBeenRegisteredErrorCode,
				ErrorDescription: errDetails,
			}
			ResponseFailed(c, err)
			return
		}
	}
	userCode := ""
	if req.Token != "" && strings.ToLower(req.Token) != "anon" {
		tokenData, err := h.AuthSvc.ValidateToken(req.Token)
		if err != nil {
			ResponseFailed(c, err)
			return
		}
		userCode = tokenData.Data[0].UserCode
	}
	responseData, err := h.UserSvc.Register(ctx, req, userCode)
	if err == err.(*user.GradeNotFoundError) {
		var errDetails []ErrorDetail
		errDetails = append(errDetails, ErrorDetail{Field: "lesson_name", Message: "lesson_name value is not allowed"})
		err = ErrorResponse{
			Status:           "error",
			ErrorType:        "lesson_name value is not allowed",
			Code:             400,
			ErrorCode:        LessonNameValueNotAllowedErrorCode,
			ErrorDescription: errDetails,
		}
		ResponseFailed(c, err)
		return
	}
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	ResponseSuccess(c, http.StatusCreated, responseData, "success")
	return
}

func (h *userHTTPHandler) CheckAvailableEmailAndCellphoneNumber(c *gin.Context) {
	var query user.GetUserQuery
	query.Email = c.PostForm("email")
	query.CellphoneNumber = c.PostForm("cellphone_number")

	if query.Email == "" {
		err := errorResponse("email", "required field")
		ResponseFailed(c, err)
		return
	}

	if query.CellphoneNumber == "" {
		err := errorResponse("cellphone_number", "required field")
		ResponseFailed(c, err)
		return
	}

	u, err := h.UserSvc.GetUsersByEmailOrCellphoneNumber(query.Email, query.CellphoneNumber)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	if (user.Users{}) == u {
		ResponseSuccessWithoutData(c, http.StatusOK, "email address and cellphone number are available")
	} else {
		result := checkUserEmailAndCellphoneNumber(query, u)
		ResponseFailed(c, result)
	}
	return
}

func checkUserEmailAndCellphoneNumber(query user.GetUserQuery, u user.Users) ErrorResponse {
	var errDetails []ErrorDetail
	if u.Email == query.Email {
		errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "Email sudah terdaftar"})
	}
	if u.CellphoneNumber == query.CellphoneNumber {
		errDetails = append(errDetails, ErrorDetail{Field: "cellphone_number", Message: "Nomor Handphone sudah terdaftar"})
	}
	result := ErrorResponse{
		Status:           "error",
		ErrorType:        "validation_fails",
		Code:             400,
		ErrorDescription: errDetails,
	}
	return result
}

func (h *userHTTPHandler) UpdateUserSubcribe(c *gin.Context) {
	subscribeReq := c.PostForm("subscribe")
	if subscribeReq == "" {
		err := errorResponse("subcribe", "required field")
		ResponseFailed(c, err)
		return
	}
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	var subscribe bool
	switch true {
	case subscribeReq == "true":
		subscribe = true
	case subscribeReq == "false":
		subscribe = false
	default:
		err := errorResponse("subcribe", "Harus dalam bentuk boolean")
		ResponseFailed(c, err)
		return
	}

	err = h.UserSvc.UpdateUserSubcribe(ctx, tokenData.Data[0].UserID, subscribe)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	ResponseSuccessWithoutData(c, http.StatusOK, "data has been saved successfully")
	return
}

func (h *userHTTPHandler) GetUserPercentage(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	userPercentage, err := h.UserSvc.GetUserPercentage(ctx, tokenData.Data[0].UserID)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	ResponseSuccessWithoutData(c, http.StatusOK, fmt.Sprintf("%v", userPercentage))
	return
}

func (h *userHTTPHandler) CheckUserParent(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	userParentRes, err := h.UserSvc.CheckUserParent(ctx, tokenData.Data[0].UserID)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	result := user.UserParentStatus{
		Status: userParentRes,
	}

	resultBackwardCompability := user.UserParentStatusBackward{
		Data: user.StatusData{
			StatusData: userParentRes,
		},
	}

	ResponseSuccessWithResult(c, http.StatusOK, result, resultBackwardCompability)
	return
}

func (h *userHTTPHandler) CreateUserParent(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	parentName := c.PostForm("parent_name")
	parentCellphoneNumber := c.PostForm("parent_cellphone_number")

	if parentName == "" {
		err := errorResponse("parent_name", "required field")
		ResponseFailed(c, err)
		return
	}

	if parentCellphoneNumber == "" {
		err := errorResponse("parent_cellphone_number", "required field")
		ResponseFailed(c, err)
		return
	}
	userID := tokenData.Data[0].UserID
	uParent := user.UserParent{
		UserID:                userID,
		ParentName:            parentName,
		ParentCellphoneNumber: parentCellphoneNumber,
		CreatedAt:             time.Now().UTC().Add(time.Hour * 7),
	}

	userParentRes, err := h.UserSvc.CheckUserParent(ctx, userID)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	if !userParentRes {
		err = h.UserSvc.CreateUserParent(ctx, uParent)
		if err != nil {
			ResponseFailed(c, err)
			return
		}
	}

	result := user.UserParentStatus{
		Status: true,
	}

	resultBackwardCompability := user.UserParentStatusBackward{
		Data: user.StatusData{
			StatusData: true,
		},
	}

	ResponseSuccessWithResult(c, http.StatusOK, result, resultBackwardCompability)
	return
}

func (h *userHTTPHandler) CheckUserActive(c *gin.Context) {
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	u, err := h.UserSvc.CheckUserActivation(tokenData.Data[0].UserID)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	ResponseSuccess(c, http.StatusOK, u, "success")
	return
}

func (h *userHTTPHandler) ResendActivation(c *gin.Context) {
	email := c.Query("email")

	if email == "" {
		err := errorResponse("email", "required field")
		ResponseFailed(c, err)
		return
	}

	err := h.UserSvc.ResendActivation(email)
	if err != nil {
		errMessage := fmt.Sprintf("%v", err)
		var statusCode int
		if errMessage == "user has been activated" {
			statusCode = http.StatusUnprocessableEntity
		} else {
			statusCode = http.StatusBadRequest
		}
		ResponseError(c, statusCode, errMessage)
		return
	}
	ResponseSuccessWithoutData(c, http.StatusOK, "email has been send")
	return
}

func (h *userHTTPHandler) ChangePassword(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		ResponseFailed(c, err)
		return
	}

	var query user.ChangePasswordReq
	query.NewPasswordConfirmation = c.PostForm("new_password_confirmation")
	query.NewPassword = c.PostForm("new_password")
	query.OldPassword = c.PostForm("old_password")

	validate := changePasswordValidate(query)
	if len(validate.ErrorDescription) != 0 {
		ResponseFailed(c, validate)
		return
	}

	userID := tokenData.Data[0].UserID
	err = h.UserSvc.ChangePassword(ctx, userID, query)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, nil, "success")
	return
}

func changePasswordValidate(query user.ChangePasswordReq) ErrorResponse {
	var errDetails []ErrorDetail
	if query.NewPassword == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "new_password", Message: "required field"})
	}

	if query.NewPasswordConfirmation == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "new_password_confirmation", Message: "required field"})
	}

	if query.OldPassword == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "old_password", Message: "required field"})
	}

	if len(query.NewPassword) < 6 {
		errDetails = append(errDetails, ErrorDetail{Field: "password", Message: "min length is 6"})
	}

	if query.NewPasswordConfirmation != query.NewPassword {
		errDetails = append(errDetails, ErrorDetail{Field: "new_password_confirmation", Message: "password and confirmation password not match."})
	}

	result := ErrorResponse{
		Status:           "error",
		ErrorType:        "validation_fails",
		Code:             400,
		ErrorDescription: errDetails,
	}
	return result
}

func (h *userHTTPHandler) GetForgotPassword(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)

	email := c.Query("email")
	ruker := c.Query("ruker")

	if email == "" {
		err := errorResponse("email", "required field")
		ResponseFailed(c, err)
		return
	}

	isRuker := false
	if ruker == "y" {
		isRuker = true
	}

	key, err := h.UserSvc.GetForgotPassword(ctx, email, isRuker)
	if err != nil {
		errStr := fmt.Sprintf("%v", err)
		if "Email not found" == errStr {
			err = errorResponse("email", errStr)
		}
		ResponseFailed(c, err)
		return
	}

	if isRuker {
		ResponseSuccess(c, http.StatusOK, nil, "Kami sudah mengirimkan password baru ke email kamu ya.")
		return
	}
	data := user.ForgotPasswordResp{
		KeyLostPassword: key,
	}
	ResponseSuccess(c, http.StatusOK, data, "Kami sudah mengirimkan cara mengganti kata sandi ke email kamu ya.")
	return
}

func (h *userHTTPHandler) SetForgotPassword(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)

	var query user.ForgotPasswordReq
	query.Email = c.PostForm("email")
	query.KeyLostPwd = c.PostForm("key_lost_pwd")
	query.Password = c.PostForm("password")
	query.PasswordConfirmation = c.PostForm("password_confirmation")

	validate := setForgotPasswordValidate(query)
	if len(validate.ErrorDescription) != 0 {
		ResponseFailed(c, validate)
		return
	}

	err := h.UserSvc.SetForgotPassword(ctx, query)
	if err != nil {
		errStr := fmt.Sprintf("%v", err)
		if "Email not found" == errStr {
			err = errorResponse("email", errStr)
		}
		if "key invalid" == errStr {
			err = errorResponse("key_lost_pwd", errStr)
		}
		ResponseFailed(c, err)
		return
	}
	ResponseSuccessWithoutData(c, http.StatusOK, "password successfully update")
	return
}

func setForgotPasswordValidate(query user.ForgotPasswordReq) ErrorResponse {
	var errDetails []ErrorDetail
	if query.Email == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "email", Message: "required field"})
	}

	if query.KeyLostPwd == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "key_lost_pwd", Message: "required field"})
	}

	if query.Password == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "password", Message: "required field"})
	}

	if query.PasswordConfirmation == "" {
		errDetails = append(errDetails, ErrorDetail{Field: "password_confirmation", Message: "required field"})
	}

	if len(query.Password) < 6 {
		errDetails = append(errDetails, ErrorDetail{Field: "password", Message: "min length is 6"})
	}

	if len(query.PasswordConfirmation) < 6 {
		errDetails = append(errDetails, ErrorDetail{Field: "password_confirmation", Message: "min length is 6"})
	}

	if query.PasswordConfirmation != query.Password {
		errDetails = append(errDetails, ErrorDetail{Field: "password_confirmation", Message: "password and confirmation password not match."})
	}

	result := ErrorResponse{
		Status:           "error",
		ErrorType:        "validation_fails",
		Code:             400,
		ErrorDescription: errDetails,
	}
	return result
}

func errorResponse(field string, message string) ErrorResponse {
	var errDetails []ErrorDetail
	errDetails = append(errDetails, ErrorDetail{Field: field, Message: message})
	return ErrorResponse{
		Status:           "error",
		ErrorType:        "validation_fails",
		Code:             400,
		ErrorDescription: errDetails,
	}
}
