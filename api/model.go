package api

import (
	"fmt"
	"log"
	"reflect"

	"github.com/gin-gonic/gin"
	"gitlab.com/ruangguru/source/user-auth/fault"
)

// Response is the standard response model
type Response struct {
	Status  string      `json:"status,omitempty"`
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
	Error   string      `json:"error,omitempty"`
	Meta    *Meta       `json:"meta,omitempty"`
	Code    int         `json:"code,omitempty"`
	Result  interface{} `json:"result,omitempty"`
}

// Meta contains all metadata associated with a response
type Meta struct {
	Pagination *Pagination `json:"pagination,omitempty"`
}

// Pagination contains all pagination-related info
type Pagination struct {
	Page      int `json:"page,omitempty"`
	Limit     int `json:"limit,omitempty"`
	TotalData int `json:"total_data,omitempty"`
	TotalPage int `json:"total_page,omitempty"`
}

// Error implements the Error interface
func (e ErrorResponse) Error() string {
	if len(e.ErrorDescription) > 0 {
		return fmt.Sprintf("Error %v (%v): %v - %v", e.Code, e.ErrorType, e.ErrorDescription[0].Field, e.ErrorDescription[0].Message)
	}
	return fmt.Sprintf("Error %v (%v)", e.Code, e.ErrorType)
}

// ErrorResponse is the standard response format for non-2xx responses
type ErrorResponse struct {
	Status           string        `json:"status,omitempty"`
	ErrorType        string        `json:"error,omitempty"`
	Code             int           `json:"code,omitempty"`
	ErrorCode        int           `json:"errorCode,omitempty"`
	ErrorDescription []ErrorDetail `json:"error_description,omitempty"`
}

// ErrorDetail contains the field and message
type ErrorDetail struct {
	Field   string `json:"field,omitempty"`
	Message string `json:"message,omitempty"`
}

func ResponseFromError(err error) ErrorResponse {
	log.Printf("Response Error: %v\n", err)

	switch v := err.(type) {

	case ErrorResponse:
		return v

	case fault.JSONDecodeError:
		return BadRequestError(v.Field, v.Error())

	case fault.InvalidTypeError:
		return BadRequestError(v.Field, v.Error())

	case fault.EntityNotExist:
		return BadRequestError(v.EntityName, v.Error())

	case fault.RecordNotFoundError:
		return BadRequestError(v.EntityName, v.Error())

	case fault.UnauthorizedError:
		return UnauthorizedError(v.EntityName, v.Message, v.ErrorCode)

	case fault.InvalidFormatError:
		return BadRequestError(v.Field, v.Error())

	case fault.InvalidValueError:
		return BadRequestError(v.Field, v.Error())

	default:
		return ErrorResponse{
			Status:    "error",
			ErrorType: "InternalServerError",
			Code:      500,
			ErrorDescription: []ErrorDetail{
				{
					Field:   "InternalServerError",
					Message: err.Error(),
				},
			},
		}

	}
}

func ResponseSuccess(c *gin.Context, status int, data interface{}, message string) {
	var resp Response
	s := reflect.ValueOf(data)

	if s.Kind() == reflect.Slice {
		resp = Response{
			Code:    status,
			Status:  "success",
			Data:    data,
			Message: message,
		}
	} else {
		resp = Response{
			Code:    status,
			Status:  "success",
			Data:    []interface{}{data},
			Message: message,
		}
	}

	c.JSON(status, resp)
	return
}

func ResponseSuccessWithoutData(c *gin.Context, status int, message string) {
	var resp Response

	resp = Response{
		Code:    status,
		Status:  "success",
		Message: message,
	}

	c.JSON(status, resp)
	return
}

func ResponseSuccessWithResult(c *gin.Context, status int, data interface{}, result interface{}) {
	var resp Response

	resp = Response{
		Code:   status,
		Status: "success",
		Result: result,
		Data:   []interface{}{data},
	}

	c.JSON(status, resp)
	return
}

func ResponseError(c *gin.Context, status int, message string) {
	var resp Response

	resp = Response{
		Code:    status,
		Status:  "error",
		Message: message,
	}

	c.JSON(status, resp)
	return
}

func ResponseFailed(c *gin.Context, err error) {
	resp := ResponseFromError(err)
	c.JSON(resp.Code, resp)
}
