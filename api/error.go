package api

import (
	"fmt"
	"net/http"
	"strings"

	validator "gopkg.in/go-playground/validator.v8"
)

func InternalServerError(err error) ErrorResponse {
	result := ErrorResponse{
		Status:    "error",
		ErrorType: "InternalServerError",
		Code:      500,
		ErrorDescription: []ErrorDetail{
			{
				Field:   "json",
				Message: fmt.Sprintf("%v", err),
			},
		},
	}
	return result
}

func BadRequestError(field string, message string) ErrorResponse {
	result := ErrorResponse{
		Status:    "error",
		ErrorType: "BadRequest",
		Code:      400,
		ErrorDescription: []ErrorDetail{
			{
				Field:   field,
				Message: message,
			},
		},
	}
	return result
}

func UnauthorizedError(field string, message string, errorCode int) ErrorResponse {
	result := ErrorResponse{
		Status:    "error",
		ErrorType: "Unauthorized",
		Code:      401,
		ErrorCode: errorCode,
		ErrorDescription: []ErrorDetail{
			{
				Field:   field,
				Message: message,
			},
		},
	}
	return result
}

func NotFoundError(entity string) ErrorResponse {
	result := ErrorResponse{
		Status:    "error",
		ErrorType: "NotFound",
		Code:      404,
		ErrorDescription: []ErrorDetail{
			{
				Field:   entity,
				Message: fmt.Sprintf("%v not found", entity),
			},
		},
	}
	return result
}

func ValidationError(err error) error {
	var errDetails []ErrorDetail

	valErrors, ok := err.(validator.ValidationErrors)
	if ok {
		for _, valError := range valErrors {
			errDetails = append(errDetails, ErrorDetail{
				// TODO: extract field name from string tag
				Field:   valError.Field,
				Message: valError.Tag,
			})
		}
	} else {
		errDetails = append(errDetails, ErrorDetail{
			Field:   "unknown",
			Message: err.Error(),
		})
	}

	result := ErrorResponse{
		Status:           "error",
		ErrorDescription: errDetails,
		ErrorType:        "BadRequest",
		Code:             http.StatusBadRequest,
	}

	return result
}

func WithFormError(err error) ErrorResponse {
	strs := strings.Split(err.Error(), " doesn't exist in ")
	field := strs[0]
	errDetail := ErrorDetail{
		Field:   field,
		Message: fmt.Sprintf("%s is an invalid parameter.", field),
	}

	result := ErrorResponse{
		Status:           "error",
		ErrorType:        "BadRequest",
		Code:             400,
		ErrorDescription: []ErrorDetail{errDetail},
	}

	return result
}
