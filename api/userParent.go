package api

import (
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ruangguru/source/shared-lib/go/api"
	"gitlab.com/ruangguru/source/shared-lib/go/fault"
	"gitlab.com/ruangguru/source/user-auth/client/auth"
	"gitlab.com/ruangguru/source/user-auth/config"
	"gitlab.com/ruangguru/source/user-auth/user"
	validationutil "gitlab.com/ruangguru/source/user-auth/utils/validationutil"
)

type UserParentHTTPHandler interface {
	CheckTncAgreement(c *gin.Context)
	SubmitTncAgreement(c *gin.Context)
}

type userParentHTTPHandler struct {
	UserSvc user.Service
	AuthSvc auth.Service
	config  config.Config
}

func NewUserParentHTTPHandler(userSvc user.Service, authSvc auth.Service, config config.Config) UserParentHTTPHandler {
	handler := userParentHTTPHandler{userSvc, authSvc, config}
	return &handler
}

func (h *userParentHTTPHandler) CheckTncAgreement(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	showFormReturn := &user.GetUserParentResponseData{
		ShowParentalForm:      true,
		ParentName:            "",
		ParentCellphoneNumber: "",
	}
	hideFormReturn := &user.GetUserParentResponseData{
		ShowParentalForm:      false,
		ParentName:            "",
		ParentCellphoneNumber: "",
	}
	if tokenData.Data[0].Anon {
		api.ResponseSuccess(c, http.StatusOK, hideFormReturn, "Success")
		return
	}
	userParentData, err := h.UserSvc.GetUserParentData(ctx, tokenData.Data[0].UserID)
	if err != nil {
		switch err.(type) {
		case *user.ErrUserIDNotFound:
			var up user.UserParent
			up.UserID = tokenData.Data[0].UserID
			localTime := getCurrentJakartaTime()
			up.CreatedAt = localTime
			approved := false
			up.TncApproved = &approved
			up.LastCheckParentalForm = &localTime
			h.UserSvc.CreateUserParent(ctx, up)
			api.ResponseSuccess(c, http.StatusOK, showFormReturn, "Success")
			return
		default:
			ResponseFailed(c, err)
			return
		}
	}
	showFormReturn = &user.GetUserParentResponseData{
		ShowParentalForm:      true,
		ParentName:            userParentData.ParentName,
		ParentCellphoneNumber: userParentData.ParentCellphoneNumber,
	}
	hideFormReturn = &user.GetUserParentResponseData{
		ShowParentalForm:      false,
		ParentName:            userParentData.ParentName,
		ParentCellphoneNumber: userParentData.ParentCellphoneNumber,
	}
	if userParentData.TncApproved == nil {
		//if last_check_parental_form is nil->update last_check->show form
		//else if last_check_parental_form is more than value->update->show form
		//else last check parental form is less than value->hide form
		if userParentData.LastCheckParentalForm == nil {
			errUpdating := h.UserSvc.UpdateLastCheckParentalForm(ctx, tokenData.Data[0].UserID)
			if errUpdating != nil {
				api.ResponseFailed(c, err)
				return
			}
			api.ResponseSuccess(c, http.StatusOK, showFormReturn, "Success")
			return
		}
		timeToCompare := (userParentData.LastCheckParentalForm).Add(time.Minute * time.Duration(h.config.DurationBeforeShowingParentalForm))
		if timeToCompare.Before(getCurrentJakartaTime()) {
			errUpdating := h.UserSvc.UpdateLastCheckParentalForm(ctx, tokenData.Data[0].UserID)
			if errUpdating != nil {
				api.ResponseFailed(c, err)
				return
			}
			api.ResponseSuccess(c, http.StatusOK, showFormReturn, "Success")
			return
		} else {
			api.ResponseSuccess(c, http.StatusOK, hideFormReturn, "Success")
			return
		}
	}
	if *userParentData.TncApproved == false {
		//if last_check_parental_form is nil->update last_check->show form
		//else if last_check_parental_form is more than value->update->show form
		//else last check parental form is less than value->hide form
		if userParentData.LastCheckParentalForm == nil {
			errUpdating := h.UserSvc.UpdateLastCheckParentalForm(ctx, tokenData.Data[0].UserID)
			if errUpdating != nil {
				api.ResponseFailed(c, err)
				return
			}
			api.ResponseSuccess(c, http.StatusOK, showFormReturn, "Success")
			return
		}
		timeToCompareIfTnCApprovedIsFalse := (userParentData.LastCheckParentalForm).Add(time.Minute * time.Duration(h.config.DurationBeforeShowingParentalForm))
		if timeToCompareIfTnCApprovedIsFalse.Before(getCurrentJakartaTime()) {
			errUpdating := h.UserSvc.UpdateLastCheckParentalForm(ctx, tokenData.Data[0].UserID)
			if errUpdating != nil {
				api.ResponseFailed(c, err)
				return
			}
			api.ResponseSuccess(c, http.StatusOK, showFormReturn, "Success")
			return
		} else {
			api.ResponseSuccess(c, http.StatusOK, hideFormReturn, "Success")
			return
		}
	}
	if *userParentData.TncApproved == true {
		api.ResponseSuccess(c, http.StatusOK, hideFormReturn, "Success")
		return
	}
}

func (h *userParentHTTPHandler) SubmitTncAgreement(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	token := c.GetHeader("Authorization")
	tokenData, err := h.AuthSvc.ValidateToken(token)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	var payload user.TnCSubmissioneReq
	errJSON := c.ShouldBindJSON(&payload)
	if errJSON != nil {
		err := ValidationError(errJSON)
		api.ResponseFailed(c, err)
		return
	}

	if !validationutil.ValidateCellphoneNumber(payload.ParentCellphoneNumber) {
		err := fault.ErrorDictionary(fault.WrongCellphoneNumberFormat, "Nomor HP harus dimulai dengan 62 dan minimal 10 digit angka, maksimal 17 angka")
		api.ResponseFailed(c, err)
		return
	}

	if !validateParentName(payload.ParentName) {
		err := fault.ErrorDictionary(fault.WrongParentNameFormat, "Nama orang tua tidak boleh berisikan angka")
		api.ResponseFailed(c, err)
		return
	}

	if payload.TncAgreed != "Y" {
		err := fault.ErrorDictionary(fault.TncAgreedMustBeYesToProceed, "")
		api.ResponseFailed(c, err)
		return
	}

	userParentData, err := h.UserSvc.GetUserParentData(ctx, tokenData.Data[0].UserID)
	if err != nil {
		switch err.(type) {
		case *user.ErrUserIDNotFound:
			var up user.UserParent
			up.UserID = tokenData.Data[0].UserID
			up.ParentName = payload.ParentName
			up.ParentCellphoneNumber = payload.ParentCellphoneNumber
			localTime := getCurrentJakartaTime()
			up.CreatedAt = localTime
			approved := true
			up.TncApproved = &approved
			up.LastCheckParentalForm = &localTime
			h.UserSvc.CreateUserParent(ctx, up)
			api.ResponseSuccess(c, http.StatusOK, "", "Success")
			return
		default:
			ResponseFailed(c, err)
			return
		}
		fmt.Println(userParentData)
	}
	errUpdating := h.UserSvc.UpdateTnCAgreed(ctx, tokenData.Data[0].UserID, payload.ParentName, payload.ParentCellphoneNumber)
	if errUpdating != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, "", "Success")
}

func getCurrentJakartaTime() time.Time {
	return time.Now().UTC().Add(time.Hour * 7)
}

func validateParentName(parentName string) bool {
	if ok, _ := regexp.MatchString(`\d`, parentName); !ok {
		return true
	}
	return false
}
