package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"gitlab.com/ruangguru/source/shared-lib/go/api"
	"gitlab.com/ruangguru/source/shared-lib/go/fault"
	"gitlab.com/ruangguru/source/shared-lib/go/utils/requests"
	"gitlab.com/ruangguru/source/shared-lib/go/utils/rg-context"
	"gitlab.com/ruangguru/source/user-auth/config"
	"gitlab.com/ruangguru/source/user-auth/user"
)

type AdminHTTPHandler interface {
	CreateUserRukerCMSBulk(c *gin.Context)
	GetUsersByQuery(c *gin.Context)
	GetAllUsersByQuery(c *gin.Context)
	UpdateUsers(c *gin.Context)
	ChangeUserEmail(c *gin.Context)
	GetUserById(c *gin.Context)
	UpdateUserById(c *gin.Context)
}

type adminHTTPHandler struct {
	UserSvc user.Service
	config  config.Config
}

func NewAdminHTTPHandler(userSvc user.Service, config config.Config) AdminHTTPHandler {
	handler := adminHTTPHandler{userSvc, config}
	return &handler
}

func (h *adminHTTPHandler) UpdateUserById(c *gin.Context) {
	var payload user.UpdateFormRequest
	err := c.ShouldBind(&payload)
	if err != nil {
		err = ValidationError(err)
		api.ResponseFailed(c, err)
		return
	}

	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	userId := c.Param("userID")
	respData, err := h.UserSvc.UpdateUser(ctx, payload, userId)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, respData, "success")
	return
}

func (h *adminHTTPHandler) GetUserById(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	userID := c.Param("userID")
	responseData, err := h.UserSvc.GetUserByUserIDForUserProfile(ctx, userID)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func (h *adminHTTPHandler) GetAllUsersByQuery(c *gin.Context) {
	var query user.GetAllUsersQuery
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)

	errJSON := c.ShouldBindWith(&query, binding.Form)
	if errJSON != nil {
		api.ResponseFailed(c, errJSON)
		return
	}

	resp, err := h.UserSvc.GetAllUsersByQuery(ctx, query)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}

	api.ResponseSuccess(c, http.StatusOK, resp, "Success")
}

func (h *adminHTTPHandler) CreateUserRukerCMSBulk(c *gin.Context) {
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	var payload []user.CreateUserRukerCMSBulkReq

	errJSON := c.ShouldBindJSON(&payload)
	if errJSON != nil {
		api.ResponseFailed(c, errJSON)
		return
	}

	resp, err := h.UserSvc.CreateUserRukerCMSBulk(ctx, payload)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}

	api.ResponseSuccess(c, http.StatusCreated, resp, "Success")
}

func (h *adminHTTPHandler) UpdateUsers(c *gin.Context) {
	var query []user.UpdateUsersByAdminQuery
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	if err := c.ShouldBind(&query); err != nil {
		err = fault.JSONDecodeError{err.Error()}
		api.ResponseFailed(c, err)
		return
	}
	err := h.UserSvc.UpdateUsersByAdmin(ctx, query)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, nil, "Success")
	return
}

func (h *adminHTTPHandler) GetUsersByQuery(c *gin.Context) {
	var query user.GetUserQuery
	ctx := rgContext.FromGinContext(c, h.config.InternalAPIRequestPassword)
	query.Email = c.Query("email")
	query.Name = c.Query("name")
	query.Emails = requests.QueryArray(c, "emails")
	query.UserSerials = requests.QueryArray(c, "userSerials")
	if query.Name == "" && query.Email == "" && len(query.Emails) <= 0 && len(query.UserSerials) <= 0 {
		err := fault.ErrorDictionary(fault.EmptyParamsForGetUsersByQuery, "")
		api.ResponseFailed(c, err)
		return
	}
	users, err := h.UserSvc.GetUsersByQuery(ctx, query)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, users, "Success")
	return
}

func (h *adminHTTPHandler) ChangeUserEmail(c *gin.Context) {
	var payload user.ChangeUserEmailReq
	ctx := api.FromGinContext(c, h.config.InternalAPIRequestPassword)
	if err := c.ShouldBind(&payload); err != nil {
		err := fault.ErrorDictionary(fault.GeneralJSONErrorCode, err.Error())
		api.ResponseFailed(c, err)
		return
	}
	err := h.UserSvc.ChangeUserEmail(ctx, payload)
	if err != nil {
		api.ResponseFailed(c, err)
		return
	}
	api.ResponseSuccess(c, http.StatusOK, nil, "Success")
	return
}
