package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ruangguru/source/user-auth/guestToken"
	serialutil "gitlab.com/ruangguru/source/user-auth/utils/serialutil"
)

type GuestTokenHTTPHandler interface {
	GenerateGuestToken(c *gin.Context)
}

type guestTokenHTTPHandler struct {
	GuestTokenSvc guestToken.Service
}

func NewGuestTokenHTTPHandler(guestTokenSvc guestToken.Service) GuestTokenHTTPHandler {
	handler := guestTokenHTTPHandler{
		GuestTokenSvc: guestTokenSvc,
	}
	return &handler
}

func (h *guestTokenHTTPHandler) GenerateGuestToken(c *gin.Context) {
	var req guestToken.GuestTokenReq
	if err := c.Bind(&req); err != nil {
		err = ValidationError(err)
		ResponseFailed(c, err)
		return
	}
	if checkGuestClientSecret(&req) == false {
		ResponseFailed(c, UnauthorizedError("client_id/client_secret", "client ID or client secret invalid", 60102))
		return
	}
	responseData, err := generateGuestResponseData(h, req.Role)
	if err != nil {
		ResponseFailed(c, err)
		return
	}
	ResponseSuccess(c, http.StatusOK, responseData, "success")
	return
}

func checkGuestClientSecret(a *guestToken.GuestTokenReq) bool {
	if a.ClientID == conf.ClientIDAndroid && a.ClientSecret == conf.ClientSecretAndroid {
		return true
	} else if a.ClientID == conf.ClientIDIOS && a.ClientSecret == conf.ClientSecretIOS {
		return true
	} else if a.ClientID == conf.ClientIDRuker && a.ClientSecret == conf.ClientSecretRuker {
		return true
	} else {
		return false
	}
}

func generateGuestResponseData(h *guestTokenHTTPHandler, role string) (*guestToken.ResponseData, error) {
	uniqueOrderCode := serialutil.GenerateUniqueOrderCodeFromName("USER")
	guestRefreshTokenClaims := h.GuestTokenSvc.BuildGuestRefreshTokenClaims(uniqueOrderCode, role)
	guestRefreshToken, err := h.GuestTokenSvc.GenerateGuestRefreshToken(guestRefreshTokenClaims)
	if err != nil {
		return &guestToken.ResponseData{}, err
	}
	guestTokenClaims := h.GuestTokenSvc.BuildGuestTokenClaims(guestRefreshToken, uniqueOrderCode, role)
	guestTokenString, err := h.GuestTokenSvc.GenerateGuestToken(guestTokenClaims)
	if err != nil {
		return &guestToken.ResponseData{}, err
	}
	responseData := h.GuestTokenSvc.BuildResponse(guestTokenString, guestRefreshToken, role)
	return responseData, nil
}
