package guestToken

import (
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/ruangguru/source/user-auth/config"
)

type Service interface {
	BuildGuestRefreshTokenClaims(uniqueOrderCode string, role string) jwt.MapClaims
	GenerateGuestRefreshToken(claims jwt.MapClaims) (string, error)
	BuildGuestTokenClaims(guestRefreshToken string, uniqueOrderCode string, role string) jwt.MapClaims
	GenerateGuestToken(claims jwt.MapClaims) (string, error)
	BuildResponse(token string, refreshToken string, role string) *ResponseData
}

type service struct{}

var conf = config.Get()

func NewService() Service {
	return &service{}
}

func (s *service) BuildGuestRefreshTokenClaims(uniqueOrderCode string, role string) jwt.MapClaims {
	guestRefreshTokenExpiry := getGuestRefreshTokenExpiry()
	claims := jwt.MapClaims{
		"exp":  guestRefreshTokenExpiry,
		"uoc":  uniqueOrderCode,
		"r":    role,
		"anon": true,
	}
	return claims
}

func (s *service) GenerateGuestRefreshToken(claims jwt.MapClaims) (string, error) {
	guestRefreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := guestRefreshToken.SignedString([]byte(conf.SecretKeyRefreshToken))
	return ss, err
}

func (s *service) BuildGuestTokenClaims(guestRefreshToken string, uniqueOrderCode string, role string) jwt.MapClaims {
	guestTokenExpiry := getGuestTokenExpiry()
	claims := jwt.MapClaims{
		"rt":   guestRefreshToken,
		"exp":  guestTokenExpiry,
		"uoc":  uniqueOrderCode,
		"r":    role,
		"anon": true,
	}
	return claims
}

func (s *service) GenerateGuestToken(claims jwt.MapClaims) (string, error) {
	guestToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := guestToken.SignedString([]byte(conf.SecretKeyToken))
	return ss, err
}

func (s *service) BuildResponse(token string, refreshToken string, role string) *ResponseData {
	returnData := ResponseData{
		RefreshToken: refreshToken,
		Role:         role,
		Token:        token,
	}
	return &returnData
}

func getGuestTokenExpiry() int64 {
	exp := time.Now().Local().Add(time.Hour * time.Duration(conf.TokenExpiryInHours))
	return exp.Unix()
}

func getGuestRefreshTokenExpiry() int64 {
	exp := time.Now().Local().Add(time.Hour * time.Duration(conf.RefreshTokenExpiryInHours))
	return exp.Unix()
}
