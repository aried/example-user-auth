package guestToken

type GuestTokenReq struct {
	ClientID     string `form:"clientID" binding:"required" json:"clientID"`
	ClientSecret string `form:"clientSecret" binding:"required" json:"clientSecret"`
	Role         string `form:"role" binding:"required" json:"role"`
}

type ResponseData struct {
	Token        string `json:"token"`
	RefreshToken string `json:"refreshToken"`
	Role         string `json:"role"`
}
