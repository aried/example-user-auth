package config

import (
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Port                                  int    `envconfig:"port" default:"80"`
	MysqlHost                             string `envconfig:"mysql_host" default:"104.xxx.xxx.xxx"`
	MysqlPort                             string `envconfig:"mysql_port" default:"3306"`
	MysqlUser                             string `envconfig:"mysql_user" default:"staging_user"`
	MysqlPass                             string `envconfig:"mysql_pass" default:"root-is-not-used"`
	MysqlName                             string `envconfig:"mysql_name" default:""`
	ClientIDAndroid                       string `envconfig:"client_id_android" default:""`
	ClientIDIOS                           string `envconfig:"client_id_ios" default:""`
	ClientIDRuker                         string `envconfig:"client_id_ruker" default:""`
	ClientSecretAndroid                   string `envconfig:"client_secret_android" default:""`
	ClientSecretIOS                       string `envconfig:"client_secret_ios" default:""`
	ClientSecretRuker                     string `envconfig:"client_secret_ruker" default:""`
	SecretKeyToken                        string `envconfig:"secret_key_token" default:""`
	SecretKeyRefreshToken                 string `envconfig:"secret_key_refresh_token" default:""`
	SecretKeyFirebaseToken                string `envconfig:"secret_key_firebase_token" default:""`
	TokenExpiryInHours                    int    `envconfig:"token_expiry_in_hours" default:""`
	RefreshTokenExpiryInHours             int    `envconfig:"refresh_token_expiry_in_hours" default:""`
	FirebaseTokenExpiryInHours            int    `envconfig:"firebase_token_expiry_in_hours" default:""`
	KongGWURL                             string `envconfig:"kong_gw_url" default:""`
	ValidateTokenEndpoint                 string `envconfig:"validate_token_endpoint" default:""`
	DBLogMode                             bool   `envconfig:"db_log_mode" default:"false"`
	UserAPISecretKey                      string `envconfig:"user_api_secret_key" default:""`
	RgBaseURL                             string `envconfig:"rg_baseurl" default:"localhost:8080"`
	InternalAPIRequestPassword            string `envconfig:"internal_api_request_password" default:""`
	DurationBeforeShowingParentalForm     int    `envconfig:"duration_before_showing_parental_form" default:""`
	IsUpdateUsersByAdminInTransactionMode bool   `envconfig:"is_update_users_by_admin_in_transaction_mode" default:""`
	KongInternalUrl                       string `envconfig:"kong_internal_url" default:""`
	SecretKeyGetProfile                   string `envconfig:"secret_key_get_profile" default:""`
	IsKoinAddedOnActivation               bool   `envconfig:"is_koin_added_on_activation" default:""`
	RgCoreSecretKey                       string `envconfig:"rg_core_secret_key" default:""`
	RgCoreHost                            string `envconfig:"rg_core_host" default:""`
	StudentClassEndpoint                  string `envconfig:"student_class_endpoint" default:"https://gw.ruangguru.com/api/v3/studentClass?classType=register"`
}

func Get() Config {
	cfg := Config{}
	envconfig.MustProcess("", &cfg)
	return cfg
}
