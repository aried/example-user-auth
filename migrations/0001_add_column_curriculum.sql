ALTER TABLE `users` 
ADD COLUMN `curriculum_serial` varchar(150) NULL AFTER `grade_serial`;

ALTER TABLE `user_extra` 
ADD COLUMN `curriculum_name` varchar(150) NULL AFTER `lesson_name`;